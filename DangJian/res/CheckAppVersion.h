//
//  CheckAppVersion.h
//  LDY
//
//  Created by 陈兵 on 17/5/18.
//  Copyright © 2017年 陈兵. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CheckAppVersion : NSObject

+ (CheckAppVersion *)sharedInstance;

- (void)isUpdataApp:(NSString *)appId;

@end
