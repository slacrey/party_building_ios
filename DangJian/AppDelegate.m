//
//  AppDelegate.m
//  DangJian
//
//  Created by 陈兵 on 2018/3/22.
//  Copyright © 2018年 chenbing. All rights reserved.
//

#import "AppDelegate.h"
//#import "CheckAppVersion.h"
#import "TabbarViewController.h"
#import "LoginViewController.h"

@interface AppDelegate ()

@property (nonatomic, strong) UINavigationController * loginViewController;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    //[[CheckAppVersion sharedInstance] isUpdataApp:@"1184767071"];

    BOOL isReadyLogin = [[DEFAULTS objectForKey:@"isReadyLogin"] boolValue];
    if (isReadyLogin==NO) {
        self.window.rootViewController = [[TabbarViewController alloc] init];
    }else{
        self.window.rootViewController = self.loginViewController;
    }
    [self.window setBackgroundColor:[UIColor blackColor]];
    [self.window makeKeyAndVisible];
    
    return YES;
}



- (UINavigationController *)loginViewController {
    
    if (_loginViewController == nil) {
        LoginViewController * loginvc = [LoginViewController new];
        UINavigationController * loginNavc = [[UINavigationController alloc] initWithRootViewController:loginvc];
        _loginViewController = loginNavc;
    }
    return _loginViewController;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
