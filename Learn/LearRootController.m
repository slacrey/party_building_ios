//
//  LearRootController.m
//  DangJian
//
//  Created by 陈兵 on 2018/3/26.
//  Copyright © 2018年 chenbing. All rights reserved.
//

#import "LearRootController.h"

@interface LearRootController ()

@end

@implementation LearRootController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = UIColorRGB(0xeeeeee);
    self.titleL.text = @"学习排名";

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
