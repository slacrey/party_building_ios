//
//  CreateWebViewController.m
//  金融
//
//  Created by Apple on 2018/1/16.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "CreateWebViewController.h"

@interface CreateWebViewController ()<UIWebViewDelegate>
@property (nonatomic, strong) UIWebView     * webView;
@end

@implementation CreateWebViewController
- (UIWebView *)webView
{
    if (!_webView) {
        _webView = [UIWebView new];
    }
    return _webView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCusNav];
    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(self.barView.mas_bottom);
    }];
    self.webView.delegate = self;
    NSURLRequest *request = [NSURLRequest requestWithURL:self.url];
    [self.webView loadRequest:request];
}
- (void)setCusNav
{
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_lessThanOrEqualTo(SCREENWIDTH - 100);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    self.titleL.text = self.cusTitle;
}

- (void)clickBackAction
{
    if([self.webView canGoBack]){
        [self.webView goBack];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
