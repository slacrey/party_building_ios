//
//  ActivityDetailViewController.m
//  金融
//
//  Created by Apple on 2018/1/3.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "ActivityDetailViewController.h"

@interface ActivityDetailViewController ()<UIWebViewDelegate>
@property (nonatomic, strong) UIWebView     * webView;
@end

@implementation ActivityDetailViewController
- (UIWebView *)webView
{
    if (!_webView) {
        _webView = [UIWebView new];
    }
    return _webView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCusNav];
    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(self.barView.mas_bottom);
    }];
    self.webView.delegate = self;
    [self networking];
}
- (void)setCusNav
{
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_lessThanOrEqualTo(SCREENWIDTH - 100);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
}
- (void)networking
{
    WeakSelf
    NSString * url = [NSString stringWithFormat:@"%@%@",HOST(@"app/activity/info/"),self.activityID];
    [HttpRequest postWithURLString:url parameters:nil showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.titleL.text = responseObject[@"data"][@"title"];
            [weakSelf.webView loadHTMLString:responseObject[@"data"][@"body"] baseURL:nil];
        });
    } failure:^(NSError * _Nullable error) {
        
    } login:^(BOOL isLogin) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf ShowLoginView];
        });
    }];
}
- (void)ShowLoginView
{
    HFLoginView * logView = [HFLoginView shareInit];
    [logView show];
    WeakSelf
    logView.clickPushBlock = ^(NSInteger index) {
        if (index == 0) {
            [weakSelf PushRigest];
        }else{
            [weakSelf PushLogin];
        }
    };
}
- (void)PushRigest
{
    RegisterViewController * rc = [[RegisterViewController alloc] init];
    rc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:rc animated:YES];
}
- (void)PushLogin
{
    LoginViewController * lgvc = [[LoginViewController alloc] init];
    lgvc.myClass = NSStringFromClass([self class]);
    WeakSelf
    lgvc.LoginSuccessBlock = ^{
        [weakSelf networking];
    };
    lgvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:lgvc animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

