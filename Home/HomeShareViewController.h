//
//  HomeShareViewController.h
//  金融
//
//  Created by Apple on 2018/1/5.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "BaseViewController.h"

@interface HomeShareViewController : BaseViewController
@property (nonatomic, copy) NSURL         * url;
@property (nonatomic, copy) NSString      * cusTitle;
@end
