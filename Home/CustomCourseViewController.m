//
//  CustomCourseViewController.m
//  金融
//
//  Created by Apple on 2018/1/3.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "CustomCourseViewController.h"
//#import "BookCell.h"
//#import "MusicListViewController.h"
//#import "KnowDetailViewController.h"
//#import "KnowCourseAllViewController.h"

static NSString * cellID = @"BookCell";
@interface CustomCourseViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) UIButton        * courseBtn;
@property (nonatomic, strong) UIButton        * informationBtn;
@property (nonatomic, strong) UICollectionView   * courseCollView;
@property (nonatomic, strong) NSMutableArray     * courseArray;
@property (nonatomic, assign) NSInteger          page;
@property (nonatomic, assign) BOOL               hasMore;
@end

@implementation CustomCourseViewController
- (NSMutableArray *)courseArray
{
    if (!_courseArray) {
        _courseArray = [NSMutableArray array];
    }
    return _courseArray;
}
- (UICollectionView *)courseCollView
{
    if (!_courseCollView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc] init]; // 自定义的布局对象
        _courseCollView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _courseCollView.dataSource = self;
        _courseCollView.delegate = self;
        
        [_courseCollView registerNib:[UINib nibWithNibName:@"BookCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:cellID];
    }
    return _courseCollView;
}

- (void)networking:(BOOL)refresh
{
    if (refresh) {
        self.page = 1;
        self.hasMore = YES;
        [self.courseArray removeAllObjects];
    }
    WeakSelf
    NSDictionary * dict = @{@"page":[NSNumber numberWithInteger:self.page],
                            @"size":[NSNumber numberWithInteger:20],
                            @"data":@{@"searchId":self.courseID}};
    [HttpRequest postWithURLString:HOST(@"app/course/list") parameters:dict showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
        [weakSelf.courseCollView.mj_header endRefreshing];
        [weakSelf.courseCollView.mj_footer endRefreshing];
        if ([responseObject[@"code"] integerValue] == CODESUCCESS) {
            for (NSDictionary * dic in responseObject[@"data"][@"content"]) {
                [weakSelf.courseArray addObject:dic];
            }
            if (weakSelf.page >= [responseObject[@"data"][@"totalPages"] integerValue] ) {
                weakSelf.hasMore = NO;
                [weakSelf.courseCollView.mj_footer endRefreshingWithNoMoreData];
                weakSelf.courseCollView.mj_footer.hidden = YES;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.courseCollView reloadData];
            });
        }
    } failure:^(NSError * _Nullable error) {
        weakSelf.courseCollView.hidden = YES;
        weakSelf.NoDataView.hidden = NO;
        [weakSelf.courseCollView.mj_footer endRefreshing];
        [weakSelf.courseCollView.mj_header endRefreshing];
        weakSelf.courseCollView.mj_footer.hidden = YES;
    }login:^(BOOL isLogin) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf ShowLoginView];
        });
    }];
}
- (void)ShowLoginView
{
    HFLoginView * logView = [HFLoginView shareInit];
    [logView show];
    WeakSelf
    logView.clickPushBlock = ^(NSInteger index) {
        if (index == 0) {
            [weakSelf PushRigest];
        }else{
            [weakSelf PushLogin];
        }
    };
}
- (void)PushRigest
{
    RegisterViewController * rc = [[RegisterViewController alloc] init];
    rc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:rc animated:YES];
}
- (void)PushLogin
{
    LoginViewController * lgvc = [[LoginViewController alloc] init];
    lgvc.myClass = NSStringFromClass([self class]);
    WeakSelf
    lgvc.LoginSuccessBlock = ^{
        [weakSelf reloadCusData];
    };
    lgvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:lgvc animated:YES];
}
- (void)reloadCusData
{
    [self networking:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = BGColor;
    [self setNavBar];
    [self addCollView];
    [self reloadCusData];
}
- (void)setNavBar
{
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    self.titleL.text = self.cusTitle;
}
- (void)addCollView
{
    [self.view addSubview:self.courseCollView];
    self.courseCollView.backgroundColor = [UIColor whiteColor];
    [self.courseCollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view);
        make.top.mas_equalTo(self.barView.mas_bottom);
        make.width.mas_offset(SCREENWIDTH);
        if (@available(iOS 11.0, *)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.mas_equalTo(self.view.mas_bottom);
        }
    }];
    self.courseCollView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.courseCollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(reloadCusData)];
}
- (void)footerRefresh
{
    if (self.hasMore) {
        self.page ++;
        [self networking:NO];
    }
}

#pragma mark - CollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.courseArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    BookCell *cell = (BookCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
//    if (self.courseArray.count > 0) {
//        [cell setDataWithDict:self.courseArray[indexPath.row]];
//    }
//    return cell;
    
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat H = (SCREENWIDTH / 3 - 30 * SS) * 1.4 + 54.5 * SS;
    return (CGSize){SCREENWIDTH / 3,H};
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0f;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    KnowDetailViewController * kv = [[KnowDetailViewController alloc] init];
//    kv.couserId = [self.courseArray[indexPath.row][@"id"] copy];
//    kv.isCouser = YES;
//    kv.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:kv animated:YES];
}
@end


