//
//  CreateWebViewController.h
//  金融
//
//  Created by Apple on 2018/1/16.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "BaseViewController.h"

@interface CreateWebViewController : BaseViewController
@property (nonatomic, copy) NSString   * cusTitle;
@property (nonatomic, copy) NSURL      * url;
@end
