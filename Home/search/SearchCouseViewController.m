//
//  SearchCouseViewController.m
//  金融
//
//  Created by Apple on 2017/10/19.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "SearchCouseViewController.h"
#import "SearchImageCell.h"
//#import "KnowDetailViewController.h"

static NSString * ImageCellID = @"SearchImageCellID";
@interface SearchCouseViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView    * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic, assign) NSInteger          page;
@property (nonatomic, assign) BOOL               hasMore;
@property (nonatomic, copy)   NSString       * name;
@end

@implementation SearchCouseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.reLoad.hidden = YES;
    self.page = 1;
    self.hasMore = YES;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        if (@available(iOS 11.0, *)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.mas_equalTo(self.view.mas_bottom);
        }
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_footer.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNameWithNoti:) name:SEARCHNOTIFI object:nil];
}
- (void)footerRefresh
{
    if (self.hasMore) {
        self.page ++;
        [self networking];
    }
}
- (void)getNameWithNoti:(NSNotification *)noti
{
    [self.dataArray removeAllObjects];
    self.page = 1;
    self.hasMore = YES;
    NSDictionary * userDict = noti.userInfo;
    self.name = userDict[@"name"];
    if ([userDict[@"index"] integerValue] == 1) {
        [self networking];
    }
}
- (void)networking
{
    WeakSelf
    NSDictionary * dict = @{@"page":[NSNumber numberWithInteger:self.page],
                            @"size":[NSNumber numberWithInteger:10],
                            @"data":@{@"name":self.name,@"type":[NSNumber numberWithInteger:0]}};
    [HttpRequest postWithURLString:HOST(@"app/course/list") parameters:dict showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue] == CODESUCCESS) {
            [weakSelf.dataArray addObjectsFromArray:responseObject[@"data"][@"content"]];
            if (weakSelf.dataArray.count == 0) {
                weakSelf.tableView.hidden = YES;
                weakSelf.NoDataView.hidden = NO;
            }else{
                weakSelf.tableView.hidden = NO;
                weakSelf.NoDataView.hidden = YES;
            }
            [weakSelf.tableView reloadData];
        }
        [weakSelf.tableView.mj_footer endRefreshing];
        if (weakSelf.page >= [responseObject[@"data"][@"totalPages"] integerValue] ) {
            weakSelf.hasMore = NO;
            [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
            weakSelf.tableView.mj_footer.hidden = YES;
            weakSelf.tableView.mj_footer.state = MJRefreshStateNoMoreData;
        }else{
            self.tableView.mj_footer.hidden = NO;
        }
    } failure:^(NSError * _Nullable error) {
        [weakSelf.tableView.mj_footer endRefreshing];
    } login:^(BOOL isLogin) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf ShowLoginView];
        });
    }];
}
- (void)ShowLoginView
{
    HFLoginView * logView = [HFLoginView shareInit];
    [logView show];
    WeakSelf
    logView.clickPushBlock = ^(NSInteger index) {
        if (index == 0) {
            [weakSelf PushRigest];
        }else{
            [weakSelf PushLogin];
        }
    };
}

- (void)PushRigest
{
    RegisterViewController * rc = [[RegisterViewController alloc] init];
    rc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:rc animated:YES];
}
- (void)PushLogin
{
    LoginViewController * lgvc = [[LoginViewController alloc] init];
    lgvc.myClass = NSStringFromClass([self class]);
    WeakSelf
    lgvc.LoginSuccessBlock = ^{
        [weakSelf reloadCusData];
    };
    lgvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:lgvc animated:YES];
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"SearchImageCell" bundle:nil] forCellReuseIdentifier:ImageCellID];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

#pragma mark - UITableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchImageCell * cell = [tableView dequeueReusableCellWithIdentifier:ImageCellID];
    [cell setDataWithDict:self.dataArray[indexPath.row]];
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    KnowDetailViewController * kc = [[KnowDetailViewController alloc] init];
//    NSString * value = self.dataArray[indexPath.row][@"id"];
//    kc.couserId = [NSString stringWithFormat:@"%@",value];
//    [self.navigationController pushViewController:kc animated:YES];
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
