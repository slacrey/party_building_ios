//
//  SearchBaseViewController.m
//  金融
//
//  Created by Apple on 2017/10/18.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "SearchBaseViewController.h"
#import "SearchInfoViewController.h"
#import "SearchCouseViewController.h"
#import "SearchImageViewController.h"

#define TextP  10
@interface SearchBaseViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) UITextField  * textF;
@end

@implementation SearchBaseViewController
- (UITextField *)textF
{
    if (!_textF) {
        _textF = [UITextField new];
        _textF.backgroundColor = UIColorRGB(0x1e64e9);
        UIView * lv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
        UIImageView *imageV =[[UIImageView alloc]initWithFrame:CGRectMake(5, 4, 14, 14)];
        [lv addSubview:imageV];
        imageV.image = [UIImage imageNamed:@"search_icon"];
        _textF.leftView = lv;
        _textF.tintColor = SELECTCOLOR;
        _textF.placeholder = @"请输入搜索名称";
        _textF.font = FONT(13);
        _textF.textColor = SELECTCOLOR;
        [_textF setValue:CONTENTFONT forKeyPath:@"_placeholderLabel.font"];
        [_textF setValue:UIColorRGB(0xc3e2fe) forKeyPath:@"_placeholderLabel.textColor"];
        _textF.layer.cornerRadius = 5.f;
        _textF.layer.masksToBounds = YES;
        UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 38, 18)];
        _textF.rightView = btn;
        btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [btn setImage:[UIImage imageNamed:@"search_clear"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(clickClearAction) forControlEvents:UIControlEventTouchUpInside];
        _textF.rightViewMode = UITextFieldViewModeAlways;
        _textF.leftViewMode = UITextFieldViewModeAlways;
        _textF.returnKeyType = UIReturnKeySearch;
        _textF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//        [_textF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _textF;
}

- (void)clickClearAction
{
    self.textF.text = @"";
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.barView addSubview:self.textF];
    [self.textF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.barView);
        make.height.mas_offset(29);
        make.left.mas_equalTo(self.backBtn.mas_right);
        make.right.mas_offset(- 15);
    }];
//    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickTap)];
//    [self.view addGestureRecognizer:tap];
    [self.textF becomeFirstResponder];
    self.textF.delegate = self;
    [self setUpAllChildViewController];
    
    [self setUpDisplayStyle:^(UIColor *__autoreleasing *titleScrollViewBgColor, UIColor *__autoreleasing *norColor, UIColor *__autoreleasing *selColor, UIColor *__autoreleasing *proColor, UIFont *__autoreleasing *titleFont, BOOL *isShowProgressView, BOOL *isOpenStretch, BOOL *isOpenShade, BOOL *isEnable ,BOOL * isShowIndex, BOOL * isTop, BOOL * isTabbar) {
        
        *isShowIndex = YES;
        *titleScrollViewBgColor = UIColorRGB(0xf2f2f2);
        *titleFont = TITLEFONT;
        *norColor = SUBTEXTColor;
        *selColor = TITLEColor;
        *proColor = TITLEColor;
        *isShowProgressView = YES;                      //是否开启标题下部Pregress指示器
        *isOpenStretch = YES;                           //是否开启指示器拉伸效果
        *isOpenShade = YES;                             //是否开启字体渐变
        *isEnable = NO;
        *isTop = YES;
        *isTabbar = NO;
    }];
    [self addObserver:self forKeyPath:@"currentIndex" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    NSLog(@"keyPath:%@",keyPath);
    NSLog(@"change：%@",change);
    NSLog(@"--value:%@",change[@"new"]);
    if (self.textF.text.length > 0 && [change[@"old"] integerValue] != [change[@"new"]integerValue]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:SEARCHNOTIFI object:nil userInfo:@{@"name":self.textF.text,@"index":[NSString stringWithFormat:@"%@",change[@"new"]]}];
        [self.textF resignFirstResponder];
    }
}

- (void)setUpAllChildViewController
{
    NSArray *titles = @[@"资讯",@"课程"];
    NSArray *vcTitle = @[@"SearchInfoViewController",@"SearchCouseViewController"];

    UIViewController *vc;
    for (NSInteger i = 0; i < titles.count; i++) {\
        vc = [[NSClassFromString(vcTitle[i]) alloc]init];
        vc.title = titles[i];
        vc.view.backgroundColor = BGColor; //随机色
        [self addChildViewController:vc];
    }
}
//- (void)clickTap
//{
//    [self.textF resignFirstResponder];
//}
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    double delayInSeconds = 0.3;
    WeakSelf
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if (textField.text.length > 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:SEARCHNOTIFI object:nil userInfo:@{@"name":textField.text,@"index":[NSString stringWithFormat:@"%ld",weakSelf.currentIndex]}];
        }
    });

    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)dealloc{
    [self removeObserver:self forKeyPath:@"currentIndex"];
}

@end
