//
//  SearchImageViewController.m
//  金融
//
//  Created by Apple on 2017/10/19.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "SearchImageViewController.h"
#import "AnalysisCell.h"
#import "HomeWebViewController.h"

static NSString * CellID = @"AnalysisCellID";
@interface SearchImageViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView    * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray;
@property (nonatomic, assign) NSInteger          page;
@property (nonatomic, assign) BOOL               hasMore;
@property (nonatomic, copy)   NSString       * name;
@end

@implementation SearchImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    self.hasMore = YES;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        if (@available(iOS 11.0, *)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.mas_equalTo(self.view.mas_bottom);
        }
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.tableView.mj_footer.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNameWithNoti:) name:SEARCHNOTIFI object:nil];
}
- (void)footerRefresh
{
    if (self.hasMore) {
        self.page ++;
        [self networking];
    }
}
- (void)getNameWithNoti:(NSNotification *)noti
{
    [self.dataArray removeAllObjects];
    NSDictionary * userDict = noti.userInfo;
    self.name = userDict[@"name"];
    if ([userDict[@"index"] integerValue] == 2) {
        [self networking];
    }
}
- (void)networking
{
    WeakSelf
    NSDictionary * dict = @{@"page":[NSNumber numberWithInteger:self.page],@"size":[NSNumber numberWithInteger:10],@"data":@{@"title":self.name}};
    [HttpRequest postWithURLString:HOST(@"app/search/analysis") parameters:dict showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue] == CODESUCCESS) {
            [weakSelf.dataArray addObjectsFromArray:responseObject[@"data"][@"content"]];
            if (weakSelf.page >= [responseObject[@"data"][@"totalPages"] integerValue] ) {
                weakSelf.hasMore = NO;
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                weakSelf.tableView.mj_footer.hidden = YES;
                weakSelf.tableView.mj_footer.state = MJRefreshStateNoMoreData;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.tableView reloadData];
            });
        }
        [weakSelf.tableView.mj_footer endRefreshing];
    } failure:^(NSError * _Nullable error) {
        NSLog(@"%@",error);
        [weakSelf.tableView.mj_footer endRefreshing];
    } login:^(BOOL isLogin) {
        [weakSelf PushLogin];
    }];
}
- (void)PushLogin
{
    LoginViewController * lgvc = [[LoginViewController alloc] init];
    lgvc.myClass = NSStringFromClass([self class]);
    WeakSelf
    lgvc.LoginSuccessBlock = ^{
        [weakSelf networking];
    };
    lgvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:lgvc animated:YES];
}
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"AnalysisCell" bundle:nil] forCellReuseIdentifier:CellID];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

#pragma mark - UITableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AnalysisCell * cell = [tableView dequeueReusableCellWithIdentifier:CellID];
    [cell setDataWithModel:self.dataArray[indexPath.row]];
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 105.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString * value = self.dataArray[indexPath.row][@"id"];
    HomeWebViewController * hc = [[HomeWebViewController alloc] init];
    hc.shareUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?id=%@",OS,@"/wap/app/analysis/info",value]];
    hc.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?token=%@&id=%@&pc=1",OS,@"/wap/app/analysis/info",[HFUser getSaveToken],value]];
    [self.navigationController pushViewController:hc animated:YES];
}
 
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

