//
//  LearnSectionViewController.h
//  金融
//
//  Created by Apple on 2018/1/17.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "BaseViewController.h"

@interface LearnSectionViewController : BaseViewController
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, copy)   NSString  *cusTitle;
@end
