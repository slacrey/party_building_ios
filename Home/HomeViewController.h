//
//  HomeViewController.h
//  financial
//
//  Created by Apple on 2017/9/21.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

//slide=幻灯片,education=教育,securityOpen=证券开户,notice=公告,icon=图标
typedef NS_ENUM(NSInteger, HomeType){
    HomeSlideType         = 0,
    HomeEducationType     = 1,
    HomeSecurityOpenType  = 2,
    HomeNoticeType        = 3,
    HomeIconType          = 4,
};
@interface HomeViewController : BaseViewController

@end
