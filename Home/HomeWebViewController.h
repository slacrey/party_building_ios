//
//  HomeWebViewController.h
//  金融
//
//  Created by Apple on 2017/10/24.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "BaseViewController.h"

@interface HomeWebViewController : BaseViewController
@property (nonatomic, copy) NSURL         * url;
@property (nonatomic, copy) NSURL         * shareUrl;
@property (nonatomic, copy) NSString      * cusTitle;
@property (nonatomic, copy) NSString      * imageUrl;
@end
