//
//  HomeModel.m
//  金融
//
//  Created by Apple on 2017/10/23.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "HomeModel.h"

@implementation HomeModel
-(instancetype)initWithDic:(NSDictionary *)dic{
    self = [super init];
    if (self) {
        if ([dic[@"moduleType"] isEqualToString:@"icon"]||[dic[@"moduleType"] isEqualToString:@"securityOpen"]) {
            self.content          = @[dic[@"content"]];
        }else{
            if ([dic[@"moduleType"] isEqualToString:@"customer"] && [dic[@"style"] integerValue] != 3) {
                self.content          = @[dic[@"content"]];
            }else{
                self.content          = dic[@"content"];
            }
        }
        self.icon             = dic[@"icon"];
        self.homeId           = dic[@"id"];
        self.isShare          = dic[@"isShare"];
        self.moduleType       = dic[@"moduleType"];
        self.sort             = dic[@"sort"];
        self.status           = dic[@"status"];
        self.style            = dic[@"style"];
        self.title            = dic[@"title"];
        self.type             = dic[@"type"];
        self.value            = dic[@"value"];
    }
    return self;
}

@end
