//
//  LSMarqueeView.h
//  LSMarqueeView
//
//  Created by WangBiao on 2017/7/19.
//  Copyright © 2017年 LSRain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LSMarqueeView : UIView

@property (nonatomic, strong) dispatch_source_t timer;

/// Label数组
@property (nonatomic, strong) NSMutableArray<UIView *> *lsViewArr;
/// 点击事件block
@property(nonatomic,strong)void (^clickBlock)(id sender);

/**
 自定义构造函数

 @param frame view frame
 @param views Array
 @return Self
 */
- (instancetype)initWithFrame:(CGRect)frame andViewArr:(NSMutableArray *)views;
/**
 开启定时器
 */
- (void)startCountdown;

@end
