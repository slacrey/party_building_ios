//
//  HomeModel.h
//  金融
//
//  Created by Apple on 2017/10/23.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeModel : NSObject
@property (nonatomic, copy) NSArray  * content;
@property (nonatomic, copy) NSDictionary  * icon;
@property (nonatomic, copy) NSString * homeId;
@property (nonatomic, copy) NSString * isShare;
@property (nonatomic, copy) NSString * moduleType;
@property (nonatomic, copy) NSString * sort;
@property (nonatomic, copy) NSString * status;
@property (nonatomic, copy) NSString * style;
@property (nonatomic, copy) NSString * title;
@property (nonatomic, copy) NSString * type;
@property (nonatomic, copy) NSString * value;
-(instancetype)initWithDic:(NSDictionary *)dic;
@end
