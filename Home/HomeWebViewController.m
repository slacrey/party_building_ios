//
//  HomeWebViewController.m
//  金融
//
//  Created by Apple on 2017/10/24.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "HomeWebViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "LLPhotoBrowser.h"
#import "WriteMessageViewController.h"
//#import <UMSocialCore/UMSocialCore.h>
//#import <UShareUI/UShareUI.h>

@interface HomeWebViewController ()<UIWebViewDelegate,LLPhotoBrowserDelegate>
@property (nonatomic, strong) UIWebView     * webView;
@property (nonatomic, strong) JSContext     * context;
@property (nonatomic, strong) UIButton      * shareBtn;
@end

@implementation HomeWebViewController
- (UIWebView *)webView
{
    if (!_webView) {
        _webView = [UIWebView new];
    }
    return _webView;
}
- (UIButton *)shareBtn
{
    if (!_shareBtn) {
        _shareBtn = [UIButton new];
        [_shareBtn setImage:[UIImage imageNamed:@"share_icon"] forState:UIControlStateNormal];
        [_shareBtn addTarget:self action:@selector(clickShareAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareBtn;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCusNav];
    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(self.barView.mas_bottom);
    }];
    self.webView.delegate = self;
    NSURLRequest *request = [NSURLRequest requestWithURL:self.url];
    [self.webView loadRequest:request];
}
- (void)setCusNav
{
//    if (self.cusTitle.length > 0) {
//        [self.barView addSubview:self.titleL];
//        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.center.mas_equalTo(self.barView);
//            make.width.mas_lessThanOrEqualTo(SCREENWIDTH - 100);
//            make.height.mas_greaterThanOrEqualTo(10);
//        }];
//        self.titleL.text = self.cusTitle;
//    }
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_lessThanOrEqualTo(SCREENWIDTH - 100);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    if (self.cusTitle.length > 0) {
        self.titleL.text = self.cusTitle;
    }else{
        self.titleL.text = @"详情";
    }
    [self.barView addSubview:self.shareBtn];
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.barView);
        make.right.mas_equalTo(self.barView.mas_right);
        make.size.mas_offset(CGSizeMake(44, 44));
    }];
}
#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView {
    [SVProgressHUD showWithStatus:@"加载中..."];
}
- (void)pushVC:(NSArray *)args
{
    WeakSelf
    NSDictionary * dict = [self dictionaryWithJsonString:[args.firstObject toString]];
//    NSLog(@"%@",dict);
    for (NSString * key in dict.allKeys) {
        if ([key isEqualToString:@"images"]) {
            NSArray * images = dict[@"images"];
            NSInteger index = [dict[@"num"] integerValue];
            LLPhotoBrowser *photoBrowser = [[LLPhotoBrowser alloc] initWithImages:images currentIndex:index];
            photoBrowser.delegate = self;
            [self presentViewController:photoBrowser animated:YES completion:nil];
            return;
        }
    }
    if ([[HFUser getSaveToken] length] > 0) {
        WriteMessageViewController * wv = [[WriteMessageViewController alloc] init];
        UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:wv];
        wv.type = WriteInfoType;
        wv.dict = dict;
        NSString *textJS = @"app.getNew()";
        wv.clickMessageBlock = ^{
            [weakSelf.context evaluateScript:textJS];
        };
        [self presentViewController:nav animated:YES completion:^{
            
        }];
    }else{
        [self ShowLoginView];
    }
}
- (void)ShowLoginView
{
    HFLoginView * logView = [HFLoginView shareInit];
    [logView show];
    WeakSelf
    logView.clickPushBlock = ^(NSInteger index) {
        if (index == 0) {
            [weakSelf PushRigest];
        }else{
            [weakSelf PushLogin];
        }
    };
}
- (void)PushRigest
{
    RegisterViewController * rc = [[RegisterViewController alloc] init];
    rc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:rc animated:YES];
}
- (void)PushLogin
{
    LoginViewController * lgvc = [[LoginViewController alloc] init];
    lgvc.myClass = NSStringFromClass([self class]);
    lgvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:lgvc animated:YES];
}
- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [SVProgressHUD dismiss];
    self.context = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    
    //  那么在OC语法中相当于 [self openNewWeb:urlStr];
    __weak typeof(self) weakSelf = self;
    self.context[@"alert"] = ^(){
        // 可以执行具体的OC代码......
        NSArray *args = [JSContext currentArguments];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf pushVC:args];
        });
    };
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
}
- (void)clickBackAction
{
    [SVProgressHUD dismiss];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)clickShareAction
{
//    [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_Sina),@(UMSocialPlatformType_QQ),@(UMSocialPlatformType_WechatSession)]];
//    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
//        UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
//
//        //创建网页内容对象
//        NSString* thumbURL = self.imageUrl;
//        UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:self.cusTitle descr:nil thumImage:thumbURL];
//        //设置网页地址
//        shareObject.webpageUrl = [self.shareUrl absoluteString];
//        //分享消息对象设置分享内容对象
//        messageObject.shareObject = shareObject;
//
//        //调用分享接口
//        [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
//            if (error) {
//                UMSocialLogInfo(@"************Share fail with error %@*********",error);
//            }else{
//                if ([data isKindOfClass:[UMSocialShareResponse class]]) {
//                    UMSocialShareResponse *resp = data;
//                    //分享结果消息
//                    UMSocialLogInfo(@"response message is %@",resp.message);
//                    //第三方原始返回的数据
//                    UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
//                }else{
//                    UMSocialLogInfo(@"response data is %@",data);
//                }
//            }
//        }];
//    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
