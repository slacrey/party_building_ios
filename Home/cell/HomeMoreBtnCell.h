//
//  HomeMoreBtnCell.h
//  金融
//
//  Created by Apple on 2017/10/23.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeModel.h"

@interface HomeMoreBtnCell : UITableViewCell

@property (nonatomic,copy) void(^ButtonClickBlock)(NSInteger index);

- (void)setDataWithModel:(HomeModel *)model;

@end
