//
//  AnalysisCell.h
//  金融
//
//  Created by Apple on 2017/9/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnalysisCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *timeL;
@property (weak, nonatomic) IBOutlet UILabel *numberL;

- (void)setDataWithModel:(NSDictionary *)dict;
@end
