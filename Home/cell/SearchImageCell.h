//
//  SearchImageCell.h
//  金融
//
//  Created by Apple on 2017/10/20.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchImageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *desL;
- (void)setDataWithDict:(NSDictionary *)dict;
@end
