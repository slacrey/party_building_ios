//
//  FinancialProductsCell.m
//  金融
//
//  Created by Apple on 2018/1/16.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "FinancialProductsCell.h"
@interface FinancialProductsCell()
@property (nonatomic, strong) UILabel      * oneL;
@property (nonatomic, strong) UILabel      * twoL;
@property (nonatomic, strong) UILabel      * threeL;
@property (nonatomic, strong) UILabel      * fourL;
@property (nonatomic, strong) UILabel      * fiveL;
@property (nonatomic, strong) UILabel      * lineL;
@property (nonatomic, strong) UIButton     * buyBtn;
@end
@implementation FinancialProductsCell
- (UIButton *)buyBtn
{
    if (!_buyBtn) {
        _buyBtn = [UIButton new];
        _buyBtn.titleLabel.font = FONT(14);
        [_buyBtn setTitle:@"立即购买" forState:UIControlStateNormal];
        _buyBtn.backgroundColor = BARColor;
    }
    return _buyBtn;
}
- (UILabel *)oneL
{
    if (!_oneL) {
        _oneL = [UILabel new];
        _oneL.font = [UIFont boldSystemFontOfSize:17];
        _oneL.textColor = TEXTColor;
    }
    return _oneL;
}
- (UILabel *)twoL
{
    if (!_twoL) {
        _twoL = [UILabel new];
        _twoL.font = [UIFont fontWithName:@"DINEngschriftStd" size:20];
        _twoL.textColor = UIColorRGB(0xfa304b);
    }
    return _twoL;
}
- (UILabel *)threeL
{
    if (!_threeL) {
        _threeL = [UILabel new];
        _threeL.font = FONT(12);
        _threeL.textColor = SUBTEXTColor;
        _threeL.text = @"单位净值";
    }
    return _threeL;
}
- (UILabel *)fourL
{
    if (!_fourL) {
        _fourL = [UILabel new];
        _fourL.font = [UIFont fontWithName:@"DINEngschriftStd" size:18];
        _fourL.textColor = UIColorRGB(0xfa304b);
    }
    return _fourL;
}
- (UILabel *)fiveL
{
    if (!_fiveL) {
        _fiveL = [UILabel new];
        _fiveL.font = FONT(12);
        _fiveL.textColor = SUBTEXTColor;
        _fiveL.text = @"近一年收益";
    }
    return _fiveL;
}
- (UILabel *)lineL
{
    if (!_lineL) {
        _lineL = [UILabel new];
        _lineL.backgroundColor = LINEColor;
    }
    return _lineL;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.oneL];
        [self.contentView addSubview:self.twoL];
        [self.contentView addSubview:self.threeL];
        [self.contentView addSubview:self.fourL];
        [self.contentView addSubview:self.fiveL];
        [self.contentView addSubview:self.lineL];
        [self.contentView addSubview:self.buyBtn];

        [self.fourL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(SCREENWIDTH * 0.5);
            make.centerY.mas_equalTo(self.contentView);
            make.width.mas_greaterThanOrEqualTo(10);
            make.height.mas_offset(20);
        }];
        [self.fiveL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(SCREENWIDTH * 0.5);
            make.top.mas_equalTo(self.fourL.mas_bottom).offset(10);
            make.width.mas_greaterThanOrEqualTo(7);
            make.height.mas_offset(14);
        }];
        
        [self.twoL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.centerY.mas_equalTo(self.contentView);
            make.right.mas_equalTo(self.fourL.mas_left).offset(-10);
            make.height.mas_offset(20);
        }];
        
        [self.oneL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.bottom.mas_equalTo(self.twoL.mas_top).offset(-10);
            make.right.mas_equalTo(self.fourL.mas_left).offset(-10);
            make.height.mas_offset(20);
        }];

        [self.threeL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.top.mas_equalTo(self.twoL.mas_bottom).offset(10);
            make.right.mas_equalTo(self.fourL.mas_left).offset(-10);
            make.height.mas_offset(14);
        }];
        [self.lineL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.right.mas_offset(-15);
            make.height.mas_offset(0.5);
            make.bottom.mas_equalTo(self.contentView);
        }];
        [self.buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_offset(-15);
            make.bottom.mas_offset(-20);
            make.size.mas_offset(CGSizeMake(75, 25));
        }];
        self.buyBtn.layer.cornerRadius = 12.5f;
        self.buyBtn.layer.masksToBounds = YES;
    }
    return self;
}
- (void)setDataWithDict:(NSDictionary *)dict index:(NSInteger)index
{
    self.buyBtn.tag = index;
    [self.buyBtn addTarget:self action:@selector(clickBuyAction:) forControlEvents:UIControlEventTouchUpInside];
    self.oneL.text = dict[@"title"];
    self.twoL.text = [NSString stringWithFormat:@"%@ (%@)",dict[@"amount"],dict[@"time"]];
    self.fourL.text = [NSString stringWithFormat:@"%@%%",dict[@"proportion"]];
}
- (void)clickBuyAction:(UIButton *)sender
{
    if (self.ClickFinancialBlock) {
        self.ClickFinancialBlock(sender.tag);
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
