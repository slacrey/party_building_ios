//
//  LearnSectionCell.h
//  金融
//
//  Created by Apple on 2018/1/17.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LearnSectionCell : UITableViewCell
@property (nonatomic, copy) void(^clickCellImgBlock)(NSInteger index);
- (void)setDataWithArray:(NSArray *)array;
@end
