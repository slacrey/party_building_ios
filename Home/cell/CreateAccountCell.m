//
//  CreateAccountCell.m
//  金融
//
//  Created by Apple on 2018/1/16.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "CreateAccountCell.h"
@interface CreateAccountCell()
@property (nonatomic, strong) UIImageView  * imageV;
@property (nonatomic, strong) UILabel      * titleL;
@property (nonatomic, strong) UILabel      * subL;
@property (nonatomic, strong) UILabel      * lineL;
@property (nonatomic, strong) UIButton     * createBtn;
@end
@implementation CreateAccountCell
- (UIImageView *)imageV
{
    if (!_imageV) {
        _imageV = [UIImageView new];
        _imageV.layer.borderColor = LINEColor.CGColor;
        _imageV.layer.borderWidth = 1.f;
    }
    return _imageV;
}
- (UILabel *)titleL
{
    if (!_titleL) {
        _titleL = [UILabel new];
        _titleL.font = FONT(17);
        _titleL.textColor = TEXTColor;
    }
    return _titleL;
}
- (UILabel *)subL
{
    if (!_subL) {
        _subL = [UILabel new];
        _subL.font = FONT(14);
        _subL.textColor = SUBTEXTColor;
    }
    return _subL;
}
- (UIButton *)createBtn
{
    if (!_createBtn) {
        _createBtn = [UIButton new];
        [_createBtn setTitle:@"立即开户" forState:UIControlStateNormal];
        _createBtn.titleLabel.font = FONT(14);
        _createBtn.backgroundColor = BARColor;
    }
    return _createBtn;
}
- (UILabel *)lineL
{
    if (!_lineL) {
        _lineL = [UILabel new];
        _lineL.backgroundColor = LINEColor;
    }
    return _lineL;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.imageV];
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.subL];
        [self.contentView addSubview:self.lineL];
        [self.contentView addSubview:self.createBtn];
        [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_offset(15);
            make.size.mas_offset(CGSizeMake(90, 90));
        }];
       
        [self.lineL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.right.mas_offset(-15);
            make.height.mas_offset(0.5);
            make.bottom.mas_equalTo(self.contentView);
        }];
        [self.createBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_offset(-20);
            make.centerY.mas_equalTo(self.contentView);
            make.size.mas_offset(CGSizeMake(75, 25));
        }];
        
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.imageV.mas_right).offset(20);
            make.top.mas_offset(20);
            make.height.mas_offset(20);
            make.right.mas_equalTo(self.createBtn.mas_left).offset(-5);
        }];
        [self.subL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.imageV.mas_right).offset(20);
            make.top.mas_equalTo(self.titleL.mas_bottom).offset(10);
            make.height.mas_offset(16);
            make.right.mas_equalTo(self.createBtn.mas_left).offset(-5);
        }];
        self.createBtn.layer.cornerRadius = 12.5f;
        self.createBtn.layer.masksToBounds = YES;
        [self.createBtn addTarget:self action:@selector(ClickCreateAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
- (void)setDataWithDict:(NSDictionary *)dict index:(NSInteger)index
{
    self.createBtn.tag = index;
    [self.imageV sd_setImageWithURL:[NSURL URLWithString:dict[@"imagePath"]]];
    self.titleL.text = dict[@"title"];
    self.subL.text = dict[@"description"];
}
- (void)ClickCreateAction:(UIButton *)sender
{
    if (self.ClickCreateBlock) {
        self.ClickCreateBlock(sender.tag);
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
