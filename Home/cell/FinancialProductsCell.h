//
//  FinancialProductsCell.h
//  金融
//
//  Created by Apple on 2018/1/16.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FinancialProductsCell : UITableViewCell
@property (nonatomic, copy) void(^ClickFinancialBlock)(NSInteger index);
- (void)setDataWithDict:(NSDictionary *)dict index:(NSInteger)index;
@end
