//
//  AnalysisCell.m
//  金融
//
//  Created by Apple on 2017/9/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "AnalysisCell.h"

@implementation AnalysisCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.titleL setLineBreakMode:NSLineBreakByTruncatingTail];
}
- (void)setDataWithModel:(NSDictionary *)dict
{
    if ([dict[@"isRead"] integerValue] == 0) {
        self.titleL.textColor = TEXTColor;
    }else{
        self.titleL.textColor = SUBTEXTColor;
    }
    NSString * str = [dict dictNullKey:@"title"];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:4.0];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    self.titleL.attributedText = attributedString;
    [self.timeL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(15);
        make.bottom.mas_offset(-15);
        make.height.mas_offset(12);
        make.width.mas_greaterThanOrEqualTo(5);
    }];
    [self.numberL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.timeL);
        make.left.mas_equalTo(self.timeL.mas_right).offset(5);
        make.height.mas_offset(12);
        make.width.mas_greaterThanOrEqualTo(5);
    }];
    if ([dict dictNullKey:@"mainImg"]) {
        NSString * str = [[dict dictNullKey:@"mainImg"] dictNullKey:@"path"];
        if (str.length == 0) {
            self.image.hidden = YES;
            [self.image mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_offset(0);
                make.right.mas_offset(0);
                make.height.mas_offset(0);
                make.width.mas_offset(0);
            }];
            [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.timeL);
                make.top.mas_offset(15);
                make.height.mas_lessThanOrEqualTo(65);
                make.right.mas_equalTo(self.contentView).offset(-15);
            }];
        }else{
            self.image.hidden = NO;
            [self.image mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(self.contentView).offset(15);
                make.right.mas_equalTo(self.contentView).offset(-15);
                make.bottom.mas_equalTo(self.contentView).offset(-15);
                make.width.mas_offset(110);
            }];
            [self.image sd_setImageWithURL:[NSURL URLWithString:str]];
            [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.timeL);
                make.top.mas_offset(15);
                make.height.mas_lessThanOrEqualTo(65);
                make.right.mas_equalTo(self.image.mas_left).offset(-10);
            }];
        }
    }

    NSString * timeStr = [NSString stringWithFormat:@"%@",dict[@"createdDateString"]];
    self.timeL.text = timeStr;
    self.numberL.text = [NSString stringWithFormat:@"%@阅读",dict[@"readNum"]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
