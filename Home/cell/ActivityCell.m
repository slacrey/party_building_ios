//
//  ActivityCell.m
//  金融
//
//  Created by Apple on 2018/1/3.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "ActivityCell.h"
@interface ActivityCell()
@property (nonatomic, strong) UIImageView     * imageV;
@property (nonatomic, strong) UILabel         * titleL;
@property (nonatomic, strong) UILabel         * timeL;
@property (nonatomic, strong) UIImageView     * endView;
@property (nonatomic, strong) UIButton        * hotBtn;
@end
@implementation ActivityCell
- (UIImageView *)imageV
{
    if (!_imageV) {
        _imageV = [UIImageView new];
    }
    return _imageV;
}
- (UILabel *)titleL
{
    if (!_titleL) {
        _titleL = [UILabel new];
        _titleL.font = FONT(17);
        _titleL.textColor = UIColorRGB(0x333333);
    }
    return _titleL;
}
- (UILabel *)timeL
{
    if (!_timeL) {
        _timeL = [UILabel new];
        _timeL.font = FONT(11);
        _timeL.textColor = UIColorRGB(0x929292);
    }
    return _timeL;
}
- (UIImageView *)endView
{
    if (!_endView) {
        _endView = [UIImageView new];
        _endView.image = [UIImage imageNamed:@"hot_end"];
    }
    return _endView;
}
- (UIButton *)hotBtn
{
    if (!_hotBtn) {
        _hotBtn = [UIButton new];
        [_hotBtn setTitleColor:UIColorRGB(0xe85e55) forState:UIControlStateNormal];
        [_hotBtn setTitleColor:UIColorRGB(0x929292) forState:UIControlStateDisabled];
        [_hotBtn setTitle:@"火热进行中" forState:UIControlStateNormal];
        _hotBtn.titleLabel.font = FONT(13);
        [_hotBtn addTarget:self action:@selector(clickHotAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _hotBtn;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.imageV];
        [self.contentView addSubview:self.titleL];
        [self.contentView addSubview:self.timeL];
        [self.contentView addSubview:self.hotBtn];
        [self.contentView addSubview:self.endView];
        [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.top.mas_offset(10);
            make.right.mas_offset(-15);
            make.height.mas_offset(150 * SizeWidth);
        }];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ClickTap:)];
        [self.imageV addGestureRecognizer:tap];
        self.imageV.userInteractionEnabled = YES;
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.top.mas_equalTo(self.imageV.mas_bottom).offset(10);
            make.right.mas_offset(-110);
            make.height.mas_offset(17);
        }];
        [self.timeL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.top.mas_equalTo(self.titleL.mas_bottom).offset(5);
            make.right.mas_offset(-110);
            make.height.mas_offset(13);
        }];
        [self.hotBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_offset(-10);
            make.width.mas_offset(85);
            make.right.mas_offset(-15);
            make.height.mas_offset(24);
        }];
        [self.endView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.hotBtn.mas_left);
            make.bottom.mas_equalTo(self.hotBtn.mas_bottom);
            make.size.mas_offset(CGSizeMake(33, 33));
        }];
    }
    return self;
}
- (void)setDataWithDict:(NSDictionary *)dict index:(NSInteger)index
{
    self.hotBtn.tag = index;
    self.imageV.tag = index;
    [self.imageV sd_setImageWithURL:[NSURL URLWithString:dict[@"mainImg"][@"path"]]];
    self.titleL.text = dict[@"title"];
    BOOL isValid = [dict[@"valid"] boolValue];
    BOOL isExpire = [dict[@"isExpire"] boolValue];
    _hotBtn.layer.cornerRadius = 5.f;
    _hotBtn.layer.borderWidth = 1.f;
    _hotBtn.layer.masksToBounds = YES;
    if (!isValid) {
        self.timeL.text = [NSString stringWithFormat:@"活动时间 %@~%@",dict[@"stringStartTime"],dict[@"stringEndTime"]];
        self.hotBtn.enabled = !isExpire;
        if (!isExpire) {
            _hotBtn.layer.borderColor = UIColorRGB(0xe85e55).CGColor;
        }else{
            _hotBtn.layer.borderColor = UIColorRGB(0x929292).CGColor;
        }
        _endView.hidden = !isExpire;
    }else{
        _endView.hidden = isValid;
        self.timeL.text = [NSString stringWithFormat:@"活动时间 %@",@"永久有效"];
        self.hotBtn.enabled = isValid;
        _hotBtn.layer.borderColor = UIColorRGB(0xe85e55).CGColor;
    }
}
- (void)clickHotAction:(UIButton *)sender
{
    if (self.clickHotAction) {
        self.clickHotAction(sender.tag);
    }
}
- (void)ClickTap:(UITapGestureRecognizer *)sender
{
    UIImageView * imageV = (UIImageView *)[sender view];
    if (self.clickHotAction) {
        self.clickHotAction(imageV.tag);
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
