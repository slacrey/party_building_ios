//
//  SearchInfoCell.h
//  金融
//
//  Created by Apple on 2017/10/19.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *timeL;
- (void)setDataWithDict:(NSDictionary *)dict;
@end
