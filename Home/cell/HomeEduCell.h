//
//  HomeEduCell.h
//  金融
//
//  Created by Apple on 2017/10/23.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeEduCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *desL;
@property (weak, nonatomic) IBOutlet UIImageView *mainV;
@property (weak, nonatomic) IBOutlet UILabel *timeL;
@property (weak, nonatomic) IBOutlet UILabel *fengL;
@property (nonatomic, strong) UIView         *starView;
- (void)setDataWithDict:(NSDictionary *)dict;
@end
