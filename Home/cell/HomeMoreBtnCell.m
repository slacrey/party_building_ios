//
//  HomeMoreBtnCell.m
//  金融
//
//  Created by Apple on 2017/10/23.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "HomeMoreBtnCell.h"
#import "HomeButton.h"

@interface HomeMoreBtnCell()

@end

@implementation HomeMoreBtnCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)setDataWithModel:(HomeModel *)model;
{
    NSArray * arr = @[@"党建资讯",@"在线学习",@"在线支部",@"知识竞答",@"党建百科",@"视频会议"];
    for (NSInteger i = 0; i < 6; i ++) {
        HomeButton *btn = [[HomeButton alloc] initWithWidth:55 * SizeWidth marginH:10];
        btn.frame = CGRectMake(i%3*(SCREENWIDTH/3) , i/3*110, SCREENWIDTH/3, 100);
        [btn setImage:[UIImage imageNamed:@"iPhone"] forState:UIControlStateNormal];
        [btn setTitle:arr[i] forState:UIControlStateNormal];
        [btn setTitleColor:UIColorHex(0x4b4a4c) forState:UIControlStateNormal];
        btn.tag = i;
        btn.titleLabel.font = FONT(14);
        [btn addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btn];
    }
}

- (void)clickButton:(UIButton *)sender
{
    if (self.ButtonClickBlock) {
        self.ButtonClickBlock(sender.tag);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
