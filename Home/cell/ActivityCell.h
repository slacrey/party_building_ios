//
//  ActivityCell.h
//  金融
//
//  Created by Apple on 2018/1/3.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityCell : UITableViewCell
@property (nonatomic, copy) void(^clickHotAction)(NSInteger index);
- (void)setDataWithDict:(NSDictionary *)dict index:(NSInteger)index;
@end
