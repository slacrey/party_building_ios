//
//  SearchInfoCell.m
//  金融
//
//  Created by Apple on 2017/10/19.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "SearchInfoCell.h"

@implementation SearchInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setDataWithDict:(NSDictionary *)dict
{
    self.titleL.text = dict[@"title"];
    self.timeL.text = dict[@"createdDateString"];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
