//
//  HomeStyleThreeCell.m
//  金融
//
//  Created by Apple on 2017/12/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "HomeStyleThreeCell.h"
@interface HomeStyleThreeCell()
@property (nonatomic, strong) UIImageView  *imagV;
@property (nonatomic, strong) UILabel      *titleL;
@property (nonatomic, strong) UILabel      *numL;
@property (nonatomic, strong) UILabel      *lineL;
@end
@implementation HomeStyleThreeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
- (UIImageView *)imagV
{
    if (!_imagV) {
        _imagV = [UIImageView new];
    }
    return _imagV;
}
- (UILabel *)titleL
{
    if (!_titleL) {
        _titleL = [UILabel new];
        _titleL.textColor = UIColorRGB(0x383838);
        _titleL.font = FONT(17);
        _titleL.numberOfLines = 2;
    }
    return _titleL;
}
- (UILabel *)numL
{
    if (!_numL) {
        _numL = [UILabel new];
        _numL.textColor = UIColorRGB(0x929292);
        _numL.font = FONT(12);
    }
    return _numL;
}
- (UILabel *)lineL
{
    if (!_lineL) {
        _lineL = [UILabel new];
        _lineL.backgroundColor = LINEColor;
    }
    return _lineL;
}
- (void)setDataWithDict:(NSDictionary *)dict
{
    [self.contentView addSubview:self.imagV];
    [self.contentView addSubview:self.titleL];
    [self.contentView addSubview:self.numL];
    [self.contentView addSubview:self.lineL];
    [self.numL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).offset(15);
        make.bottom.mas_equalTo(self.contentView).offset(-15);
        make.height.mas_offset(12);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    UIImageView * playV = [UIImageView new];
    playV.image = [UIImage imageNamed:@"BF"];
    if (dict[@"image"][@"path"]) {
        self.imagV.hidden = NO;
        [self.imagV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView).offset(15);
            make.right.mas_equalTo(self.contentView).offset(-15);
            make.bottom.mas_equalTo(self.contentView).offset(-15);
            make.width.mas_offset(110);
        }];
        if ([dict[@"type"] integerValue] == 20) {
            [self.imagV addSubview:playV];
            [playV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_offset(CGSizeMake(30, 30));
                make.center.mas_equalTo(self.imagV);
            }];
        }
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(self.contentView).offset(15);
            make.right.mas_equalTo(self.imagV.mas_left).offset(-30);
            make.bottom.mas_equalTo(self.numL.mas_top).offset(-15);
        }];
        [self.imagV sd_setImageWithURL:[NSURL URLWithString:dict[@"image"][@"path"]]];
    }else{
        self.imagV.hidden = YES;
        [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(self.contentView).offset(15);
            make.right.mas_equalTo(self.contentView).offset(-15);
            make.bottom.mas_equalTo(self.numL.mas_top).offset(-15);
        }];
    }
    
    [self.lineL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).offset(15);
        make.right.mas_equalTo(self.contentView).offset(-15);
        make.bottom.mas_equalTo(self.contentView).offset(0);
        make.height.mas_offset(0.5);
    }];
    self.titleL.text = dict[@"title"];
    self.numL.text = [NSString stringWithFormat:@"%@ %@阅读",dict[@"extra"][@"publishDate"],dict[@"extra"][@"readNum"]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
