//
//  HomeButton.m
//  Bbuilding
//
//  Created by apple on 2017/5/3.
//  Copyright © 2017年 HF. All rights reserved.
//

#import "HomeButton.h"
@interface HomeButton()
@property (nonatomic, assign) CGFloat    width;
@property (nonatomic, assign) CGFloat    marginH;
@end
@implementation HomeButton
- (instancetype)initWithWidth:(CGFloat)width marginH:(CGFloat)marginH
{
    self = [super init];
    if (self) {
        self.width = width;
        self.marginH = marginH;
    }
    return self;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if(self.titleLabel.text && self.imageView.image)
    {
        CGRect rect = self.imageView.frame;
        rect.size.width = self.width;
        rect.size.height = self.width;
        self.imageView.frame = rect;
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        CGFloat marginH = self.marginH;
        
        //图片
        CGPoint imageCenter = self.imageView.center;
        imageCenter.x = self.frame.size.width / 2;
        imageCenter.y = self.imageView.frame.size.height / 2 + marginH * 2;
        self.imageView.center = imageCenter;
        //文字
        CGRect newFrame = self.titleLabel.frame;
        newFrame.origin.x = 0;
        newFrame.origin.y = self.imageView.frame.size.height + marginH * 3;
        newFrame.size.width = self.frame.size.width;
        newFrame.size.height = 16.f;
        self.titleLabel.frame = newFrame;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
}
@end
