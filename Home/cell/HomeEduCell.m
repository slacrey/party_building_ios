//
//  HomeEduCell.m
//  金融
//
//  Created by Apple on 2017/10/23.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "HomeEduCell.h"

#define STARTAG 100
@implementation HomeEduCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.contentView addSubview:self.starView];
    [self.starView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.fengL);
        make.left.mas_equalTo(self.timeL.mas_right);
        make.right.mas_equalTo(self.fengL.mas_left).offset(-3);
        make.height.mas_offset(14);
    }];
    for (NSInteger i = 0; i < 5; i ++) {
        UIImageView * imageV = [[UIImageView alloc] init];
        [self.starView addSubview:imageV];
        [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_offset(CGSizeMake(10, 10));
            make.right.mas_equalTo(self.fengL.mas_left).offset(-i * 13);
            make.centerY.mas_equalTo(self.starView);
        }];
        imageV.tag = 4 - i + STARTAG;
        imageV.image = [UIImage imageNamed:@"xing_kong"];
    }
}
- (UIView *)starView
{
    if (!_starView) {
        _starView = [UIView new];
    }
    return _starView;
}
- (void)setDataWithDict:(NSDictionary *)dict
{
    [self.mainV sd_setImageWithURL:[NSURL URLWithString:dict[@"image"][@"path"]]];
    self.desL.text = dict[@"title"];
    self.fengL.hidden = YES;
    self.starView.hidden = YES;
//    self.fengL.text = [NSString stringWithFormat:@"%@",dict[@"extra"][@"rate"]];
    self.timeL.text = [NSString stringWithFormat:@"%@",dict[@"extra"][@"publishDate"]];;
//    CGFloat rate = [dict[@"extra"][@"rate"] floatValue];
//
//    for (NSInteger i = 0; i < 5; i ++) {
//        UIImageView * imageV = (UIImageView *)[self.starView viewWithTag:STARTAG + i];
//        imageV.image = [UIImage imageNamed:@"xing_kong"];
//    }
//    BOOL isBan = ((long)(rate * 10) / 5)%2 == 1 ? YES : NO;
//    NSInteger starN = (NSInteger)rate + (isBan ? 1 : 0);
//    if (starN > 0) {
//        for (NSInteger i = 0; i < starN - 1; i ++) {
//            UIImageView * imageV = (UIImageView *)[self.starView viewWithTag:STARTAG + i];
//            imageV.image = [UIImage imageNamed:@"xing_man"];
//        }
//        UIImageView * imageV = (UIImageView *)[self.starView viewWithTag:STARTAG + starN - 1];
//        if (isBan) {
//            imageV.image = [UIImage imageNamed:@"xing_ban"];
//        }else{
//            imageV.image = [UIImage imageNamed:@"xing_man"];
//        }
//    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
