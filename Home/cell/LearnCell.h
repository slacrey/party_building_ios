//
//  LearnCell.h
//  金融
//
//  Created by Apple on 2018/1/17.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LearnCell : UITableViewCell
- (void)setDataWithDict:(NSDictionary *)dict;
@end
