//
//  SearchImageCell.m
//  金融
//
//  Created by Apple on 2017/10/20.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "SearchImageCell.h"

@implementation SearchImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setDataWithDict:(NSDictionary *)dict
{
    NSLog(@"%@",dict);
    [self.iconV sd_setImageWithURL:[NSURL URLWithString:dict[@"mainImg"][@"path"]]];
    self.titleL.text = dict[@"name"];
    self.desL.text = dict[@"description"];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
