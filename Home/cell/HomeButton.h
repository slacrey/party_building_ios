//
//  HomeButton.h
//  Bbuilding
//
//  Created by apple on 2017/5/3.
//  Copyright © 2017年 HF. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeButton : UIButton
- (instancetype)initWithWidth:(CGFloat)width marginH:(CGFloat)marginH;
@end
