//
//  HomeStyleThreeCell.h
//  金融
//
//  Created by Apple on 2017/12/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeModel.h"

@interface HomeStyleThreeCell : UITableViewCell

- (void)setDataWithDict:(NSDictionary *)dict;

@end
