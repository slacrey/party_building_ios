//
//  LearnCell.m
//  金融
//
//  Created by Apple on 2018/1/17.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "LearnCell.h"

@interface LearnCell()
@property (nonatomic, strong) UIImageView    * imageV;
@end
@implementation LearnCell
- (UIImageView *)imageV
{
    if (!_imageV) {
        _imageV = [UIImageView new];
        _imageV.layer.cornerRadius = 5.f;
        _imageV.layer.masksToBounds = YES;
    }
    return _imageV;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.imageV];
        [self.imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_offset(15);
            make.right.mas_offset(-15);
            make.top.mas_offset(10 * SizeWidth);
            make.height.mas_offset(188 * SizeWidth);
        }];
    }
    return self;
}
- (void)setDataWithDict:(NSDictionary *)dict
{
    [self.imageV sd_setImageWithURL:[NSURL URLWithString:dict[@"image"][@"path"]]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
