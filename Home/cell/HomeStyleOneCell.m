//
//  HomeStyleOneCell.m
//  金融
//
//  Created by Apple on 2017/12/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "HomeStyleOneCell.h"
@interface HomeStyleOneCell()
@end
@implementation HomeStyleOneCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
- (void)setDataWithArray:(NSArray *)array
{
    CGFloat W = (SCREENWIDTH - 40) / 2;
    CGFloat H = 95 * SizeWidth;

    for (NSInteger i = 0; i < [array count]; i ++) {
        UIImageView * imgV = [UIImageView new];
        imgV.tag = i;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImg:)];
        [imgV addGestureRecognizer:tap];
        imgV.userInteractionEnabled = YES;
        
        UIImageView * playV = [UIImageView new];
        playV.image = [UIImage imageNamed:@"BF"];
        
        UILabel * label = [UILabel new];
        label.font = FONT(14);
        label.textColor = UIColorRGB(0x2e2e2e);
        [self.contentView addSubview:imgV];
        [self.contentView addSubview:label];
        if (i % 2 == 0) {
            if (i / 2 == 0 ) {
                [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_offset(15);
                    make.top.mas_offset(10);
                    make.size.mas_offset(CGSizeMake(W,H));
                }];
            }else{
                [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_offset(15);
                    make.top.mas_offset(10 + i / 2 * (H + 35));
                    make.size.mas_offset(CGSizeMake(W,H));
                }];
            }
        }else{
            if (i / 2 == 0 ) {
                [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.mas_offset(-15);
                    make.top.mas_offset(10);
                    make.size.mas_offset(CGSizeMake(W,H));
                }];
            }else{
                [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.mas_offset(-15);
                    make.top.mas_offset(10 + i / 2 * (H + 35));
                    make.size.mas_offset(CGSizeMake(W,H));
                }];
            }
        }
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(imgV.mas_left);
            make.top.mas_equalTo(imgV.mas_bottom).offset(8);
            make.width.mas_equalTo(imgV.mas_width);
            make.height.mas_offset(14);
        }];
        if (i + 1 == [array count]) {
            UIView * bgView = [UIView new];
            [self.contentView addSubview:bgView];
            bgView.backgroundColor = [UIColor whiteColor];
            [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.contentView);
                make.top.mas_equalTo(label.mas_bottom);
                make.size.mas_offset(CGSizeMake(SCREENWIDTH, 10));
            }];
        }
        if ([array[i][@"type"] integerValue] ==20) {
            [imgV addSubview:playV];
            [playV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_offset(CGSizeMake(30, 30));
                make.center.mas_equalTo(imgV);
            }];
        }
        [imgV sd_setImageWithURL:[NSURL URLWithString:array[i][@"image"][@"path"]]];
        label.text = array[i][@"title"];
    }
}
- (void)setDataWithModel:(HomeModel *)model
{
    CGFloat W = (SCREENWIDTH - 40) / 2;
    CGFloat H = 95 * SizeWidth;
    
    NSArray * array = [model.content firstObject];
    for (NSInteger i = 0; i < [array count]; i ++) {
        UIImageView * imgV = [UIImageView new];
        imgV.tag = i;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImg:)];
        [imgV addGestureRecognizer:tap];
        imgV.userInteractionEnabled = YES;
  
        UIImageView * playV = [UIImageView new];
        playV.image = [UIImage imageNamed:@"BF"];
        
        UILabel * label = [UILabel new];
        label.font = FONT(14);
        label.textColor = UIColorRGB(0x2e2e2e);
        [self.contentView addSubview:imgV];
        [self.contentView addSubview:label];
        if (i % 2 == 0) {
            if (i / 2 == 0 ) {
                [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_offset(15);
                    make.top.mas_offset(10);
                    make.size.mas_offset(CGSizeMake(W,H));
                }];
            }else{
                [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_offset(15);
                    make.top.mas_offset(10 + i / 2 * (H + 35));
                    make.size.mas_offset(CGSizeMake(W,H));
                }];
            }
        }else{
            if (i / 2 == 0 ) {
                [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.mas_offset(-15);
                    make.top.mas_offset(10);
                    make.size.mas_offset(CGSizeMake(W,H));
                }];
            }else{
                [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.right.mas_offset(-15);
                    make.top.mas_offset(10 + i / 2 * (H + 35));
                    make.size.mas_offset(CGSizeMake(W,H));
                }];
            }
        }
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(imgV.mas_left);
            make.top.mas_equalTo(imgV.mas_bottom).offset(8);
            make.width.mas_equalTo(imgV.mas_width);
            make.height.mas_offset(14);
        }];
        if (i + 1 == [model.content count]) {
            UIView * bgView = [UIView new];
            [self.contentView addSubview:bgView];
            bgView.backgroundColor = [UIColor whiteColor];
            [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(self.contentView);
                make.top.mas_equalTo(label.mas_bottom);
                make.size.mas_offset(CGSizeMake(SCREENWIDTH, 10));
            }];
        }
        if ([array[i][@"type"] integerValue] ==20) {
            [imgV addSubview:playV];
            [playV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_offset(CGSizeMake(30, 30));
                make.center.mas_equalTo(imgV);
            }];
        }
        [imgV sd_setImageWithURL:[NSURL URLWithString:array[i][@"image"][@"path"]]];
        label.text = array[i][@"title"];
    }
}

- (void)clickImg:(UITapGestureRecognizer *)sender
{
    UIImageView * imgv = (UIImageView *)[sender view];
    if (self.clickCellImgBlock) {
        self.clickCellImgBlock(imgv.tag);
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
