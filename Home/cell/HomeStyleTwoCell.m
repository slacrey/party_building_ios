//
//  HomeStyleTwoCell.m
//  金融
//
//  Created by Apple on 2017/12/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "HomeStyleTwoCell.h"

@implementation HomeStyleTwoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        
    }
    return self;
}
- (void)setDataWithModel:(HomeModel *)model
{
    CGFloat W = (SCREENWIDTH - 90) / 3;
    CGFloat H = W * 1.4;
    NSArray * array = [model.content firstObject];
    for (NSInteger i = 0; i < [array count]; i ++) {
        UIImageView * imgV = [UIImageView new];
        [self.contentView addSubview:imgV];
        imgV.tag = i;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImg:)];
        [imgV addGestureRecognizer:tap];
        imgV.userInteractionEnabled = YES;
        
        UIImageView * playV = [UIImageView new];
        playV.image = [UIImage imageNamed:@"BF"];
        
        UILabel * label = [UILabel new];
        label.font = FONT(14);
        label.textColor = UIColorRGB(0x383838);
        label.textAlignment = NSTextAlignmentCenter;
        
        [self.contentView addSubview:label];
        [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.contentView).offset(15 + (i % 3 * (30 + W)));
            make.top.mas_equalTo(self.contentView).offset(i / 3 * (H + 34) + 10);
            make.size.mas_offset(CGSizeMake(W, H));
        }];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(imgV.mas_left);
            make.right.mas_equalTo(imgV.mas_right);
            make.top.mas_equalTo(imgV.mas_bottom).offset(10);
            make.height.mas_equalTo(14);
        }];
        if ([array[i][@"type"] integerValue] ==20) {
            [imgV addSubview:playV];
            [playV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_offset(CGSizeMake(30, 30));
                make.center.mas_equalTo(imgV);
            }];
        }
        [imgV sd_setImageWithURL:[NSURL URLWithString:array[i][@"image"][@"path"]]];
        label.text = array[i][@"title"];
    }
}

- (void)clickImg:(UITapGestureRecognizer *)sender
{
    UIImageView * imgv = (UIImageView *)[sender view];
    if (self.clickCellImgBlock) {
        self.clickCellImgBlock(imgv.tag);
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
