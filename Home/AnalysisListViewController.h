//
//  AnalysisListViewController.h
//  金融
//
//  Created by Apple on 2017/12/28.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "BaseViewController.h"

@interface AnalysisListViewController : BaseViewController
@property (nonatomic, copy) NSString    * value;
@property (nonatomic, copy) NSString    * cusTitle;
@end
