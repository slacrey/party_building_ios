//
//  CreateAccountViewController.m
//  金融
//
//  Created by Apple on 2018/1/16.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "CreateAccountViewController.h"
#import "CreateAccountCell.h"
#import "CreateWebViewController.h"

static NSString * CellID = @"CreateAccountCell";
@interface CreateAccountViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView    * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray;
@end

@implementation CreateAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBar];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.barView.mas_bottom);
        make.left.right.mas_equalTo(self.view);
        if (@available(iOS 11.0, *)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.mas_equalTo(self.view.mas_bottom);
        }
    }];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self networking];
}

- (void)setNavBar
{
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_lessThanOrEqualTo(SCREENWIDTH - 100);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    self.titleL.text = @"证券开户";
}
- (void)networking
{
    WeakSelf
    [HttpRequest postWithURLString:HOST(@"app/securityOpen/list") parameters:nil showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue] == CODESUCCESS) {
            [weakSelf.dataArray addObjectsFromArray:responseObject[@"data"]];
            if (weakSelf.dataArray.count == 0) {
                weakSelf.tableView.hidden = YES;
                weakSelf.NoDataView.hidden = NO;
            }else{
                weakSelf.tableView.hidden = NO;
                weakSelf.NoDataView.hidden = YES;
            }
            [weakSelf.tableView reloadData];
        }
    } failure:^(NSError * _Nullable error) {

    } login:^(BOOL isLogin) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [weakSelf ShowLoginView];
//        });
    }];
}
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[CreateAccountCell class] forCellReuseIdentifier:CellID];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

#pragma mark - UITableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WeakSelf
    CreateAccountCell * cell = [[CreateAccountCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setDataWithDict:self.dataArray[indexPath.row] index:indexPath.row];
    cell.ClickCreateBlock = ^(NSInteger index) {
        [weakSelf pushCreateAccount:weakSelf.dataArray[indexPath.row]];
    };
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 115.f;
}
- (void)pushCreateAccount:(NSDictionary *)dict
{
    switch ([dict[@"id"] integerValue]) {
        case 0:
        {
            CreateWebViewController * cv = [[CreateWebViewController alloc] init];
            cv.cusTitle = dict[@"title"];
            cv.url = [NSURL URLWithString:@"http://m.dgzq.com.cn/kh/m/open/index.html"];
            [self.navigationController pushViewController:cv animated:YES];
        }
            break;
        case 1:
        {
            
        }
            break;
        default:
        {
            CreateWebViewController * cv = [[CreateWebViewController alloc] init];
            cv.cusTitle = dict[@"title"];
            cv.url = [NSURL URLWithString:@"http://m.dgzq.com.cn/kh/m/open/index.html"];
            [self.navigationController pushViewController:cv animated:YES];
        }
            break;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
