//
//  LearnListViewController.m
//  金融
//
//  Created by Apple on 2018/1/17.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "LearnListViewController.h"
#import "LearnCell.h"
#import "LearnSectionViewController.h"

static NSString * CellID = @"LearnCellID";
@interface LearnListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView    * tableView;
@property (nonatomic, strong) NSMutableArray * dataArray;
@end

@implementation LearnListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBar];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.barView.mas_bottom);
        make.left.right.mas_equalTo(self.view);
        if (@available(iOS 11.0, *)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.mas_equalTo(self.view.mas_bottom);
        }
    }];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self networking];
}

- (void)setNavBar
{
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_lessThanOrEqualTo(SCREENWIDTH - 100);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    self.titleL.text = @"学股堂";
}
- (void)networking
{
    WeakSelf
    [HttpRequest postWithURLString:HOST(@"app/stageCourse/guideList") parameters:nil showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue] == CODESUCCESS) {
            weakSelf.dataArray = responseObject[@"data"];
            if (weakSelf.dataArray.count == 0) {
                weakSelf.tableView.hidden = YES;
                weakSelf.NoDataView.hidden = NO;
            }else{
                weakSelf.tableView.hidden = NO;
                weakSelf.NoDataView.hidden = YES;
            }
            [weakSelf.tableView reloadData];
        }
    } failure:^(NSError * _Nullable error) {
        
    } login:^(BOOL isLogin) {
        //        dispatch_async(dispatch_get_main_queue(), ^{
        //            [weakSelf ShowLoginView];
        //        });
    }];
}
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[LearnCell class] forCellReuseIdentifier:CellID];
    }
    return _tableView;
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

#pragma mark - UITableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LearnCell * cell = [[LearnCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellID];
    [cell setDataWithDict:self.dataArray[indexPath.row]];
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 198 * SizeWidth;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    LearnSectionViewController * ls = [[LearnSectionViewController alloc] init];
    ls.cusTitle = self.dataArray[indexPath.row][@"title"];
    ls.type = [self.dataArray[indexPath.row][@"value"] integerValue];
    [self.navigationController pushViewController:ls animated:YES];
}
@end
