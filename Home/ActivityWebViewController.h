//
//  ActivityWebViewController.h
//  金融
//
//  Created by Apple on 2018/1/3.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "BaseViewController.h"

@interface ActivityWebViewController : BaseViewController
@property (nonatomic, copy)   NSURL     * url;
@property (nonatomic, copy)   NSString  * cusTitle;
@end
