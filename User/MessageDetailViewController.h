//
//  MessageDetailViewController.h
//  金融
//
//  Created by Apple on 2017/11/15.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "BaseViewController.h"

@interface MessageDetailViewController : BaseViewController
@property (nonatomic, copy) NSString * messageID;
@end
