//
//  ChangeInfoViewController.h
//  金融
//
//  Created by Apple on 2017/10/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "BaseViewController.h"
typedef NS_ENUM(NSInteger, ChangeInfoType)
{
    ChangeInfoAvatar = 0,
    ChangeInfoName = 1,
};
@interface ChangeInfoViewController : BaseViewController
@property (nonatomic, copy) void(^ClickChangeInfoBlock)(void);
@end
