//
//  MessageViewController.m
//  金融
//
//  Created by Apple on 2017/9/30.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "MessageViewController.h"
#import "MessageCell.h"
#import "MessageDetailViewController.h"

static NSString * cellID = @"MessageCellID";
@interface MessageViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView        * tableView;
@property (nonatomic, strong) NSMutableArray     * dataArray;
@property (nonatomic, assign) NSInteger          page;
@property (nonatomic, assign) BOOL               hasMore;
@end

@implementation MessageViewController
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"MessageCell" bundle:nil] forCellReuseIdentifier:cellID];
    }
    return _tableView;
}
- (void)networking
{
    WeakSelf
    NSDictionary * dic = @{@"page":[NSNumber numberWithInteger:self.page],
                           @"size":[NSNumber numberWithInteger:20]
                           };
    [HttpRequest postWithURLString:HOST(@"app/message/list") parameters:dic showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue] == CODESUCCESS) {
            for (NSDictionary * dict in responseObject[@"data"][@"content"]) {
                [weakSelf.dataArray addObject:dict];
            }
            [weakSelf.tableView.mj_footer endRefreshing];
            if (weakSelf.page == [responseObject[@"data"][@"totalPages"] integerValue] ) {
                weakSelf.hasMore = NO;
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                weakSelf.tableView.mj_footer.hidden = YES;
            }
            if (weakSelf.dataArray.count > 0) {
               [weakSelf.tableView reloadData];
                weakSelf.tableView.hidden = NO;
                weakSelf.NoDataView.hidden = YES;
            }else{
                weakSelf.tableView.hidden = YES;
                weakSelf.NoDataView.hidden = NO;
            }
        }
    } failure:^(NSError * _Nullable error) {
        weakSelf.tableView.hidden = YES;
        weakSelf.NoDataView.hidden = NO;
        weakSelf.tableView.mj_footer.hidden = YES;
    }login:^(BOOL isLogin) {
        [weakSelf.tableView.mj_footer endRefreshing];
    }];
}
- (void)reloadCusData
{
    self.page = 1;
    self.hasMore = YES;
    [self.dataArray removeAllObjects];
    [self networking];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    self.titleL.text = @"消息";
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.barView.mas_bottom);
        if (@available(iOS 11.0, *)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.mas_equalTo(self.view.mas_bottom);
        }
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.page = 1;
    self.hasMore = YES;
    [self networking];
}
- (void)footerRefresh
{
    if (self.hasMore) {
        self.page ++;
        [self networking];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageCell * cell = (MessageCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
    [cell setDataWithDict:self.dataArray[indexPath.row]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MessageDetailViewController * mv = [[MessageDetailViewController alloc] init];
    mv.messageID = self.dataArray[indexPath.row][@"id"];
    [self.navigationController pushViewController:mv animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

