//
//  SettingViewController.m
//  金融
//
//  Created by Apple on 2017/9/29.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "SettingViewController.h"
#import "SettingCell.h"
#import "AboutViewController.h"

static NSString * cellID = @"SettingCellID";
@interface SettingViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSArray     * dataArray;
@end

@implementation SettingViewController
- (NSArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = @[@"关于我们",@"清除缓存",@"退出登录"];
    }
    return _dataArray;
}
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"SettingCell" bundle:nil] forCellReuseIdentifier:cellID];
    }
    return _tableView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    self.titleL.text = @"设置";
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(self.barView.mas_bottom);
    }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingCell * cell = (SettingCell *)[tableView dequeueReusableCellWithIdentifier:cellID];
//    UILabel * lineL= [UILabel new];
//    lineL.backgroundColor = LINEColor;
//    [cell.contentView addSubview:lineL];
//    [lineL mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_offset(15);
//        make.right.mas_offset(0);
//        make.bottom.mas_offset(-1);
//        make.height.mas_offset(1);
//    }];
    [cell setDataWithText:self.dataArray[indexPath.row]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
        {
            AboutViewController * ac = [[AboutViewController alloc] init];
            [self.navigationController pushViewController:ac animated:YES];
        }
            break;
        case 1:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"清除缓存?" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = 0;
            [alert show];
        }
            break;
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"确定退出登录?" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = 1;
            [alert show];
        }
            break;
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    WeakSelf
    if (alertView.tag == 0) {
        [[SDImageCache sharedImageCache] clearDisk];
    }else{
        if (buttonIndex == 1) {
            [HttpRequest getWithURLString:HOST(@"auth/logout") parameters:nil showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
                NSLog(@"%@",responseObject);
                if ([responseObject[@"code"] integerValue] == CODESUCCESS) {
                    [HFUser deleteToken];
                    [HFUser deleteIcon];
                    if (weakSelf.clickLogOut) {
                        weakSelf.clickLogOut();
                    }
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
            } failure:^(NSError * _Nullable error) {
                NSLog(@"%@",error);
            }];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
