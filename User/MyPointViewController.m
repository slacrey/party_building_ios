//
//  MyPointViewController.m
//  金融
//
//  Created by Apple on 2017/10/30.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "MyPointViewController.h"

@interface MyPointViewController ()
@property (nonatomic, strong) UIView  * headView;
@property (nonatomic, strong) UIView  * RadiusView;
@property (nonatomic, strong) UILabel * pointL;
@property (nonatomic, strong) UILabel * desL;
@property (nonatomic, strong) UIButton* qianBtn;
@end

@implementation MyPointViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavbar];
    [self setMyPointView];
    [self networking];
}
- (void)setNavbar
{
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    self.titleL.text = @"我的积分";
}
- (UIView *)headView
{
    if (!_headView) {
        _headView = [[UIView alloc] init];
        _headView.backgroundColor = [UIColor whiteColor];
    }
    return _headView;
}
- (UIView *)RadiusView
{
    if (!_RadiusView) {
        _RadiusView = [[UIView alloc] init];
        _RadiusView.layer.cornerRadius = 52.5f;
        _RadiusView.layer.borderWidth = 4.f;
        _RadiusView.layer.borderColor = [UIColor orangeColor].CGColor;
        _RadiusView.layer.masksToBounds = YES;
    }
    return _RadiusView;
}
- (UILabel *)pointL
{
    if (!_pointL) {
        _pointL = [UILabel new];
        _pointL.textAlignment = NSTextAlignmentCenter;
        _pointL.font = [UIFont boldSystemFontOfSize:28];
    }
    return _pointL;
}
- (UILabel *)desL
{
    if (!_desL) {
        _desL = [UILabel new];
        _desL.textAlignment = NSTextAlignmentCenter;
        _desL.font = INSFONT;
        _desL.textColor = SUBTEXTColor;
        _desL.text = @"积分总数";
    }
    return _desL;
}
- (UIButton *)qianBtn
{
    if (!_qianBtn) {
        _qianBtn = [UIButton new];
        [_qianBtn setTitle:@"每日签到" forState:UIControlStateNormal];
        _qianBtn.titleLabel.font = FONT(14);
        [_qianBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_qianBtn setBackgroundColor:UIColorRGB(0xd1bb99)];
        _qianBtn.layer.cornerRadius = 5.f;
        _qianBtn.layer.masksToBounds = YES;
    }
    return _qianBtn;
}
- (void)setMyPointView
{
    [self.view addSubview:self.headView];
    [self.headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.barView.mas_bottom);
        make.height.mas_offset(255);
    }];
    [self.headView addSubview:self.RadiusView];
    [self.RadiusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.headView);
        make.size.mas_offset(CGSizeMake(105, 105));
    }];
    [self.RadiusView addSubview:self.pointL];
    [self.pointL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.RadiusView);
        make.top.mas_equalTo(self.RadiusView.mas_top).offset(34);
        make.size.mas_offset(CGSizeMake(88, 22));
    }];
    [self.RadiusView addSubview:self.desL];
    [self.desL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.RadiusView);
        make.top.mas_equalTo(self.pointL.mas_bottom).offset(5);
        make.size.mas_offset(CGSizeMake(70, 14));
    }];
    [self.headView addSubview:self.qianBtn];
    [self.qianBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.headView);
        make.size.mas_offset(CGSizeMake(80, 41));
        make.top.mas_equalTo(self.RadiusView.mas_bottom).offset(20);
    }];
    [self.qianBtn addTarget:self action:@selector(clickQianAction) forControlEvents:UIControlEventTouchUpInside];
}
- (void)networking
{
    WeakSelf
    [HttpRequest postWithURLString:HOST(@"app/user/point") parameters:nil showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue] == CODESUCCESS) {
            weakSelf.pointL.text = [NSString stringWithFormat:@"%@",responseObject[@"data"][@"points"]];
        }
    } failure:^(NSError * _Nullable error) {
        
    } login:^(BOOL isLogin) {

    }];
}
- (void)clickQianAction
{
    WeakSelf
    [HttpRequest postWithURLString:HOST(@"app/sign/in") parameters:nil showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue] == CODESUCCESS) {
            [weakSelf networking];
        }
    } failure:^(NSError * _Nullable error) {
        
    } login:^(BOOL isLogin) {
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
