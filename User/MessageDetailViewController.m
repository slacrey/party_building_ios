//
//  MessageDetailViewController.m
//  金融
//
//  Created by Apple on 2017/11/15.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "MessageDetailViewController.h"

@interface MessageDetailViewController ()<UIWebViewDelegate>
@property (nonatomic, strong) UIWebView  * webView;
@end

@implementation MessageDetailViewController
- (UIWebView *)webView
{
    if (!_webView) {
        _webView = [UIWebView new];
    }
    return _webView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_greaterThanOrEqualTo(10);
    }];

    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(self.barView.mas_bottom);
    }];
    self.webView.delegate = self;
    [self networking];
}
- (void)networking
{
    WeakSelf
    NSString * url = [NSString stringWithFormat:@"%@%@",HOST(@"app/message/info/"),self.messageID];
    [HttpRequest postWithURLString:url parameters:nil showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
        weakSelf.titleL.text = responseObject[@"data"][@"title"];
        [weakSelf.webView loadHTMLString:responseObject[@"data"][@"content"] baseURL:nil];
    } failure:^(NSError * _Nullable error) {
        
    } login:^(BOOL isLogin) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
