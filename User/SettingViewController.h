//
//  SettingViewController.h
//  金融
//
//  Created by Apple on 2017/9/29.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingViewController : BaseViewController
@property (nonatomic,copy) void (^clickLogOut)(void);
@end
