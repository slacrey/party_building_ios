//
//  MessageCell.m
//  金融
//
//  Created by Apple on 2017/9/30.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "MessageCell.h"

@implementation MessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setDataWithDict:(NSDictionary *)dict
{
    [self.iconV sd_setImageWithURL:[NSURL URLWithString:dict[@"message"][@"icon"]]];
    if ([dict[@"isRead"] integerValue] == 0) {
        self.titleL.textColor = TEXTColor;
    }else{
        self.titleL.textColor = SUBTEXTColor;
    }
    self.titleL.text = dict[@"message"][@"title"];
    self.subTitle.text = dict[@"message"][@"content"];
    self.timeL.text = dict[@"message"][@"publishDate"];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
