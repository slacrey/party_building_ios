//
//  SettingCell.h
//  金融
//
//  Created by Apple on 2017/9/29.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UIImageView *jianV;
- (void)setDataWithText:(NSString *)title;
@end
