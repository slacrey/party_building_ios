//
//  UserIconView.m
//  Bbuilding
//
//  Created by apple on 2017/4/29.
//  Copyright © 2017年 HF. All rights reserved.
//

#import "UserIconView.h"
@interface UserIconView()
@property (nonatomic, strong) UIImageView   * iconV;
@property (nonatomic, strong) UIButton      * nameButton;
@property (nonatomic, strong) UILabel       * usreL;
//@property (nonatomic, strong) UIButton      * button;
@end
@implementation UserIconView
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setFrame:CGRectMake(0, 0, SCREENWIDTH, 164.f * SizeScale)];
        [self setup];
    }
    return self;
}
- (UIImageView *)iconV
{
    if (!_iconV) {
        _iconV = [UIImageView new];
        _iconV.userInteractionEnabled = YES;
    }
    return _iconV;
}
- (UIButton *)nameButton
{
    if (!_nameButton) {
        _nameButton = [UIButton new];
    }
    return _nameButton;
}
- (UILabel *)usreL
{
    if (!_usreL) {
        _usreL = [UILabel new];
    }
    return _usreL;
}
//- (UIButton *)button
//{
//    if (!_button) {
//        _button = [UIButton new];
//    }
//    return _button;
//}
- (void)setup
{
    self.layer.contents = (__bridge id _Nullable)([UIImage imageNamed:@"User_bg"].CGImage);
//
//    UIImageView * imageV = [UIImageView new];
//    [self addSubview:imageV];
//
//    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self).offset(24);
//        make.size.mas_equalTo(CGSizeMake(88, 88));
//        make.centerX.mas_equalTo(self);
//    }];
//    imageV.layer.cornerRadius = 44;
//    imageV.layer.borderWidth = 3.f;
//    imageV.layer.borderColor = [UIColor whiteColor].CGColor;
//    imageV.layer.masksToBounds = YES;
//    imageV.userInteractionEnabled = YES;
    [self addSubview:self.iconV];
    UITapGestureRecognizer * iconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickIcon)];
    [self.iconV addGestureRecognizer:iconTap];
    
    [self addSubview:self.nameButton];
    [self addSubview:self.usreL];
//    [self addSubview:self.button];
    
    [self.iconV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).offset(24);
        make.size.mas_equalTo(CGSizeMake(88,88));
        make.centerX.mas_equalTo(self);
    }];
    self.iconV.layer.cornerRadius = 44;
    self.iconV.layer.masksToBounds = YES;
    
    
    [self.nameButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.iconV.mas_bottom).offset(10 * SizeScale);
        make.centerX.mas_equalTo(self);
        make.width.mas_greaterThanOrEqualTo(1);
        make.height.mas_offset(@(18 * SizeScale));
    }];
    [self.nameButton addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    [self.nameButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.usreL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.nameButton.mas_bottom).offset(3 * SizeScale);
        make.centerX.mas_equalTo(self);
        make.width.mas_greaterThanOrEqualTo(1);
        make.height.mas_offset(@(14 * SizeScale));
    }];
    self.usreL.textAlignment = NSTextAlignmentCenter;
    self.usreL.font = FONT(12);
    self.usreL.textColor = UIColorHex(0xa6a6a6);
    
//    [self.button mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self).offset(70 * SizeScale);
//        make.left.mas_equalTo(self).offset(SCREENWIDTH * 0.5 + 25 * SizeScale);
//        make.size.mas_offset(CGSizeMake(20 * SizeScale, 20 * SizeScale));
//    }];
//    [self.button setImage:[UIImage imageNamed:@"bj"] forState:UIControlStateNormal];
//    [self.button addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
//    [self.button bringSubviewToFront:self];
}
//- (void)clickAction:(UIButton *)sender
//{
//    if (self.ClickBlock) {
//        self.ClickBlock(sender.tag);
//    }
//}
- (void)clickIcon
{
    if (self.ClickBlock) {
        self.ClickBlock(100);
    }
}
- (void)loginAction
{
    if (self.LoginBlock) {
        self.LoginBlock();
    }
}
- (void)setIconImage:(UIImage *)iconImage
{
    self.iconV.image = iconImage;
}
- (void)setDataWithUserIcon
{
    if ([[HFUser getSaveToken] length] > 0) {
        if ([HFUser getIcon]) {
            UIImage * newImage = [self imageFromImage:[HFUser getIcon]];
            self.iconV.image = newImage;
        }
        if ([[HFUser lastNickName] length] > 0) {
            self.nameButton.enabled = NO;
            [self.nameButton setTitle:[HFUser lastNickName] forState:UIControlStateDisabled];
        }
    }else{
        self.iconV.image = [UIImage imageNamed:@"icon_p"];
        self.nameButton.enabled = YES;
        [self.nameButton setTitle:@"登录/注册" forState:UIControlStateNormal];
    }
}
- (UIImage *)imageFromImage:(UIImage *)image
{
    //将UIImage转换成CGImageRef
    CGImageRef sourceImageRef = [image CGImage];
    CGFloat W = image.size.height > image.size.width ? image.size.width : image.size.height;
    CGRect rect = CGRectMake(0, (image.size.height - W)/2,W,W);
    //按照给定的矩形区域进行剪裁
    CGImageRef newImageRef = CGImageCreateWithImageInRect(sourceImageRef, rect);
    
    //将CGImageRef转换成UIImage
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    //返回剪裁后的图片
    return newImage;
}

@end
