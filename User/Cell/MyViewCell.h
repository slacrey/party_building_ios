//
//  MyViewCell.h
//  金融
//
//  Created by Apple on 2017/9/28.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageV;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UIImageView *jianV;
- (void)setDataWithDict:(NSDictionary *)dict;
@end
