//
//  UserInfoNameCell.m
//  金融
//
//  Created by Apple on 2017/10/30.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "UserInfoNameCell.h"

@implementation UserInfoNameCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.jianV.tintColor = SUBTEXTColor;
}
- (void)setDataWithDict:(NSDictionary *)dict isHidden:(BOOL)isHidden
{
    self.titleL.text = dict[@"title"];
    self.nameL.hidden = isHidden;
    self.nameL.text = [HFUser lastNickName];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
