//
//  UserCountCell.m
//  Bbuilding
//
//  Created by apple on 2017/4/29.
//  Copyright © 2017年 HF. All rights reserved.
//

#import "UserCountCell.h"

@implementation UserCountCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        NSArray * array = @[@"总资产",@"总成本",@"总收益"];
        for (NSInteger i = 0; i < 3; i ++) {
            UIView * view = [UIView new];
            [self.contentView addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.mas_equalTo(self);
                make.left.equalTo(self).offset(i * SCREENWIDTH / 3);
                make.width.mas_offset(@(SCREENWIDTH / 3));
            }];
            UILabel * numL = [UILabel new];
            numL.textAlignment = NSTextAlignmentCenter;
            numL.textColor = RedColor;
            numL.font = [UIFont systemFontOfSize:27];
            [view addSubview:numL];
            [numL mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(view).offset(14);
                make.left.right.mas_equalTo(view);
                make.height.mas_offset(@26);
            }];
            UILabel * descriptionL = [UILabel new];
            [view addSubview:descriptionL];
            descriptionL.textAlignment = NSTextAlignmentCenter;
            descriptionL.textColor = TEXTColor;
            descriptionL.font = [UIFont systemFontOfSize:14];
            descriptionL.text = array[i];
            [descriptionL mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(numL.mas_bottom).offset(5);
                make.left.right.mas_equalTo(view);
                make.height.mas_offset(20);
            }];
            switch (i) {
                case 0:
                    _keepL = numL;
                    break;
                case 1:
                    _countL = numL;
                    break;
                default:
                    _useL = numL;
                    break;
            }
        }
    }
    return self;
}
- (void)setUserCountWithArray:(NSArray *)array
{
    _keepL.text = [NSString stringWithFormat:@"%@",array[0]];
    _countL.text = [NSString stringWithFormat:@"%@",array[1]];
    _useL.text = [NSString stringWithFormat:@"%@",array[2]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
