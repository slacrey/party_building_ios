//
//  UserInfoImageCell.m
//  金融
//
//  Created by Apple on 2017/10/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "UserInfoImageCell.h"

@implementation UserInfoImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.jianV.tintColor = SUBTEXTColor;
}
- (void)setDataWithDict:(NSDictionary *)dict
{
    self.titleL.text = dict[@"title"];
    self.iconV.image = [self imageFromImage:[self getImage]];
}
- (UIImage *)getImage
{
    NSString *path_document = NSHomeDirectory();
    
    NSString *imagePath = [path_document stringByAppendingString:@"/Documents/icon.png"];
    
    UIImage *getimage = [UIImage imageWithContentsOfFile:imagePath];
    
    return getimage;
}
- (UIImage *)imageFromImage:(UIImage *)image
{
    //将UIImage转换成CGImageRef
    CGImageRef sourceImageRef = [image CGImage];
    CGFloat W = image.size.height > image.size.width ? image.size.width : image.size.height;
    CGRect rect = CGRectMake(0, (image.size.height - W)/2,W,W);
    //按照给定的矩形区域进行剪裁
    CGImageRef newImageRef = CGImageCreateWithImageInRect(sourceImageRef, rect);
    
    //将CGImageRef转换成UIImage
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    //返回剪裁后的图片
    return newImage;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
