//
//  UserCountCell.h
//  Bbuilding
//
//  Created by apple on 2017/4/29.
//  Copyright © 2017年 HF. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserCountCell : UITableViewCell
@property (nonatomic, strong) UILabel   * keepL;
@property (nonatomic, strong) UILabel   * countL;
@property (nonatomic, strong) UILabel   * useL;
- (void)setUserCountWithArray:(NSArray *)array;
@end
