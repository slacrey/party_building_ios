//
//  MyViewCell.m
//  金融
//
//  Created by Apple on 2017/9/28.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "MyViewCell.h"

@implementation MyViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.jianV.tintColor = SUBTEXTColor;
}
- (void)setDataWithDict:(NSDictionary *)dict
{
    self.iconImageV.image = [UIImage imageNamed:dict[@"image"]];
    self.titleL.text = dict[@"title"];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
