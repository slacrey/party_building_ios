//
//  BookCell.m
//  金融
//
//  Created by Apple on 2017/9/29.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "BookCell.h"

@interface BookCell()
@property (nonatomic, strong) UILabel * free;
@end
@implementation BookCell
- (UILabel *)free
{
    if (!_free) {
        _free = [[UILabel alloc] init];
        _free.text = @"免费";
        _free.textAlignment = NSTextAlignmentCenter;
        _free.backgroundColor = RedColor;
        _free.textColor = [UIColor whiteColor];
        _free.font = FONT(14);
    }
    return _free;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor whiteColor];
    [self.bookImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_offset(15 * SS);
        make.right.mas_offset(-15 * SS);
        make.height.mas_equalTo(self.bookImage.mas_width).multipliedBy(1.4);
    }];
    [self.bookImage addSubview:self.free];
    [self.free mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bookImage);
        make.right.mas_equalTo(self.bookImage.right).offset(0);
        make.size.mas_offset(CGSizeMake(30, 20));
    }];
    self.bookName.numberOfLines = 2;
    self.bookName.font = FONT(12);
    self.bookName.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.bookName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(20 * SS);
        make.right.mas_offset(-20 * SS);
        make.height.mas_lessThanOrEqualTo(32 * SS);
        make.top.mas_equalTo(self.bookImage.mas_bottom).offset(7.5 * SS);
    }];
}
- (void)setDataWithDict:(NSDictionary *)dict
{
    if ([dict[@"type"] integerValue] == 90000) {
        self.free.hidden = YES;
        self.bookName.hidden = YES;
        self.bookImage.image = [UIImage imageNamed:@"add_book"];
    }else{
        self.bookName.hidden = NO;
        if ([dict[@"free"] integerValue] == 0) {
            self.free.hidden = YES;
        }else{
            self.free.hidden = NO;
        }
        [self.bookImage sd_setImageWithURL:[NSURL URLWithString:dict[@"mainImg"][@"path"]] placeholderImage:[UIImage imageNamed:@"book_place"]];
        self.bookName.text = dict[@"name"];
    }
}
@end
