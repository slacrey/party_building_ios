//
//  UserIconView.h
//  Bbuilding
//
//  Created by apple on 2017/4/29.
//  Copyright © 2017年 HF. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserIconView : UIView
@property (nonatomic, strong) UIImage   * iconImage;
@property (nonatomic, copy) void(^LoginBlock)(void);
@property (nonatomic, copy) void(^ClickBlock)(NSInteger index);
- (void)setDataWithUserIcon;
- (void)setIconImage:(UIImage *)iconImage;
@end
