//
//  UserInfoNameCell.h
//  金融
//
//  Created by Apple on 2017/10/30.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInfoNameCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *nameL;
@property (weak, nonatomic) IBOutlet UIImageView *jianV;
- (void)setDataWithDict:(NSDictionary *)dict isHidden:(BOOL)isHidden;
@end
