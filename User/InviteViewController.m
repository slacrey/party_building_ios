//
//  InviteViewController.m
//  金融
//
//  Created by Apple on 2017/10/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "InviteViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>
//#import <UMSocialCore/UMSocialCore.h>
//#import <UShareUI/UShareUI.h>

@interface InviteViewController ()<UIWebViewDelegate>
@property (nonatomic, strong) UIWebView     * webView;
@property (nonatomic, strong) JSContext     * context;
@end

@implementation InviteViewController
- (UIWebView *)webView
{
    if (!_webView) {
        _webView = [UIWebView new];
    }
    return _webView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCusNav];
    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(self.barView.mas_bottom);
    }];
    self.webView.delegate = self;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@?token=%@",OS,@"/wap/app/activity/dynamic/invite",[HFUser getSaveToken]]]];
    [self.webView loadRequest:request];
}

- (void)setCusNav
{
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_lessThanOrEqualTo(SCREENWIDTH - 100);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    self.titleL.text = @"邀请有礼";
}
#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView {
    [SVProgressHUD showWithStatus:@"加载中..."];
}
- (void)pushVC:(NSArray *)args
{
    NSDictionary * dict = [self dictionaryWithJsonString:[args.firstObject toString]];
    if ([dict[@"type"] isEqualToString:@"share"]) {
        [self shareInfo:dict[@"target"]];
    }
}
- (void)shareInfo:(NSDictionary *)dict
{
//    [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_Sina),@(UMSocialPlatformType_QQ),@(UMSocialPlatformType_WechatSession)]];
//    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
//        UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
//
//        if ([dict[@"name"] isEqualToString:@"web"]) {
//            NSString* thumbURL = dict[@"config"][@"image"];
//            UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:dict[@"config"][@"title"] descr:dict[@"config"][@"body"] thumImage:thumbURL];
//            //设置网页地址
//            shareObject.webpageUrl = dict[@"config"][@"url"];
//            messageObject.shareObject = shareObject;
//        }else if ([dict[@"name"] isEqualToString:@"content"]){
//            UMShareObject *shareObject = [UMShareObject shareObjectWithTitle:dict[@"config"][@"title"] descr:dict[@"config"][@"body"] thumImage:dict[@"config"][@"image"]];
//            messageObject.shareObject = shareObject;
//        }else if ([dict[@"name"] isEqualToString:@"image"]){
//            UMShareImageObject *shareObject = [[UMShareImageObject alloc] init];
//            //如果有缩略图，则设置缩略图
//            shareObject.thumbImage = dict[@"config"][@"image"];
//            [shareObject setShareImage:dict[@"config"][@"image"]];
//            messageObject.shareObject = shareObject;
//        }
//        //调用分享接口
//        [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
//            if (error) {
//                UMSocialLogInfo(@"************Share fail with error %@*********",error);
//            }else{
//                if ([data isKindOfClass:[UMSocialShareResponse class]]) {
//                    UMSocialShareResponse *resp = data;
//                    //分享结果消息
//                    UMSocialLogInfo(@"response message is %@",resp.message);
//                    //第三方原始返回的数据
//                    UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
//                }else{
//                    UMSocialLogInfo(@"response data is %@",data);
//                }
//            }
//        }];
//    }];
}
- (void)ShowLoginView
{
    HFLoginView * logView = [HFLoginView shareInit];
    [logView show];
    WeakSelf
    logView.clickPushBlock = ^(NSInteger index) {
        if (index == 0) {
            [weakSelf PushRigest];
        }else{
            [weakSelf PushLogin];
        }
    };
}
- (void)PushRigest
{
    RegisterViewController * rc = [[RegisterViewController alloc] init];
    rc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:rc animated:YES];
}
- (void)PushLogin
{
    LoginViewController * lgvc = [[LoginViewController alloc] init];
    lgvc.myClass = NSStringFromClass([self class]);
    lgvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:lgvc animated:YES];
}
- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [SVProgressHUD dismiss];
    self.context = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    
    //  那么在OC语法中相当于 [self openNewWeb:urlStr];
    __weak typeof(self) weakSelf = self;
    self.context[@"alert"] = ^(){
        // 可以执行具体的OC代码......
        NSArray *args = [JSContext currentArguments];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf pushVC:args];
        });
    };
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
}
- (void)clickBackAction
{
    [SVProgressHUD dismiss];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

