

//
//  FaceBookViewController.m
//  金融
//
//  Created by Apple on 2017/10/9.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "FaceBookViewController.h"
#import "PlaceholderTextView.h"

@interface FaceBookViewController ()<UITextViewDelegate>
@property (nonatomic, strong) PlaceholderTextView  * textView;
@property (nonatomic, strong) UIButton             * submitBtn;
@property (nonatomic, copy)   NSString             * content;
@end

@implementation FaceBookViewController
- (UIButton *)submitBtn
{
    if (!_submitBtn) {
        _submitBtn = [[UIButton alloc] init];
        [_submitBtn setTitle:@"提交" forState:UIControlStateNormal];
        [_submitBtn addTarget:self action:@selector(clickSumbitAction) forControlEvents:UIControlEventTouchUpInside];
        _submitBtn.layer.cornerRadius = 5.f;
        _submitBtn.layer.masksToBounds = YES;
        [_submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_submitBtn setBackgroundColor:BARColor];
    }
    return _submitBtn;
}
- (PlaceholderTextView *)textView
{
    if (!_textView) {
        _textView = [[PlaceholderTextView alloc] init];
        _textView.backgroundColor = [UIColor whiteColor];
        _textView.layer.cornerRadius = 5.f;
        _textView.layer.masksToBounds = YES;
        _textView.placeholderLabel.font = CONTENTFONT;
        _textView.placeholder = @"请反馈您的宝贵建议";
        _textView.font = CONTENTFONT;
        _textView.returnKeyType = UIReturnKeyDone;
        _textView.maxLength = 300;
    }
    return _textView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.height.mas_offset(20);
        make.width.mas_greaterThanOrEqualTo(10);
    }];
    self.titleL.text = @"反馈";
    [self.view addSubview:self.textView];
    self.textView.delegate = self;
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(15);
        make.top.mas_equalTo(self.barView.mas_bottom).offset(15);
        make.right.mas_offset(-15);
        make.height.mas_offset(250);
    }];
    WeakSelf
    [self.textView didChangeText:^(PlaceholderTextView *textView) {
        weakSelf.content = textView.text;
    }];
    [self.view addSubview:self.submitBtn];
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(15);
        make.right.mas_offset(-15);
        make.top.mas_equalTo(self.textView.mas_bottom).mas_offset(37);
        make.height.mas_offset(40);
    }];
}
- (void)clickSumbitAction
{
    [self.textView resignFirstResponder];
    if (self.content.length == 0) {
        return;
    }
    WeakSelf
    [HttpRequest postWithURLString:HOST(@"app/feedback") parameters:@{@"body":self.content} showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
        weakSelf.content = @"";
        [MyObject failedPrompt1:responseObject[@"message"]];
        if ([responseObject[@"code"] integerValue] == CODESUCCESS) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf.navigationController popViewControllerAnimated:YES];
            });
        }
    } failure:^(NSError * _Nullable error) {
        NSLog(@"%@",error);
    }login:^(BOOL isLogin) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf ShowLoginView];
        });
    }];
}
- (void)ShowLoginView
{
    HFLoginView * logView = [HFLoginView shareInit];
    [logView show];
    WeakSelf
    logView.clickPushBlock = ^(NSInteger index) {
        if (index == 0) {
            [weakSelf PushRigest];
        }else{
            [weakSelf PushLogin];
        }
    };
}
- (void)PushRigest
{
    RegisterViewController * rc = [[RegisterViewController alloc] init];
    rc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:rc animated:YES];
}
- (void)PushLogin
{
    LoginViewController * lgvc = [[LoginViewController alloc] init];
    lgvc.myClass = NSStringFromClass([self class]);
    lgvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:lgvc animated:YES];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        [textView resignFirstResponder];
    }
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
