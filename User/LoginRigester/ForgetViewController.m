//
//  ForgetViewController.m
//  金融
//
//  Created by Apple on 2017/9/28.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "ForgetViewController.h"

#define MAX_STARWORDS_LENGTH 6
@interface ForgetViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) UIButton    *  yanBtn;
@end

@implementation ForgetViewController
- (UIButton *)yanBtn
{
    if (!_yanBtn) {
        _yanBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
        [_yanBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_yanBtn setTitleColor:RedColor forState:UIControlStateNormal];
        _yanBtn.contentMode = UIViewContentModeCenter;
        _yanBtn.titleLabel.font = FONT(15);
        [_yanBtn addTarget:self action:@selector(clickYanBtn) forControlEvents:UIControlEventTouchUpInside];
    }
    return _yanBtn;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    if([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
    [self setNavBar];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickTap)];
    [self.view addGestureRecognizer:tap];
    self.phoneTF.delegate = self;
    self.yanTF.delegate = self;
    self.yanTF.keyboardType = UIKeyboardTypeNumberPad;
    self.yanTF.rightView = self.yanBtn;
    self.yanTF.rightViewMode = UITextFieldViewModeAlways;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textFieldEditChanged:)
                                                name:UITextFieldTextDidChangeNotification object: self.yanTF];
}
- (void)setNavBar
{
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    self.titleL.text = @"短信登录";
}
-(void)textFieldEditChanged:(NSNotification *)obj
{
    UITextField *textField = (UITextField *)obj.object;
    NSString *toBeString = textField.text;
    NSString *lang = [textField.textInputMode primaryLanguage];
    if ([lang isEqualToString:@"zh-Hans"])// 简体中文输入
    {
        //获取高亮部分
        UITextRange *selectedRange = [textField markedTextRange];
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position)
        {
            if (toBeString.length > MAX_STARWORDS_LENGTH)
            {
                textField.text = [toBeString substringToIndex:MAX_STARWORDS_LENGTH];
            }
        }
        
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else
    {
        if (toBeString.length > MAX_STARWORDS_LENGTH)
        {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:MAX_STARWORDS_LENGTH];
            if (rangeIndex.length == 1)
            {
                textField.text = [toBeString substringToIndex:MAX_STARWORDS_LENGTH];
            }
            else
            {
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, MAX_STARWORDS_LENGTH)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
}

- (void)clickYanBtn
{
    if (self.phoneTF.text.length != 11) {
        [MyObject failedPrompt1:@"手机号码不正确"];
        return;
    }
    [HttpRequest postWithURLString:HOST(@"common/sms/send") parameters:@{@"phone":self.phoneTF.text,@"type":[NSNumber numberWithInteger:0]} showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
        [MyObject failedPrompt1:responseObject[@"message"]];
        if ([responseObject[@"code"] integerValue] == 20000) {
            __block NSInteger timeout = 60; //倒计时时间
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
            dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
            dispatch_source_set_event_handler(_timer, ^{
                if(timeout<=0){ //倒计时结束，关闭
                    dispatch_source_cancel(_timer);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //设置界面的按钮显示 根据自己需求设置
                        self.yanBtn.backgroundColor = [UIColor whiteColor];
                        [self.yanBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
                        [self.yanBtn setTitleColor:TITLEColor forState:UIControlStateNormal];
                        self.yanBtn.userInteractionEnabled = YES;
                    });
                }else{
                    int seconds = timeout % 61;
                    NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //设置界面的按钮显示 根据自己需求设置
                        [self.yanBtn setTitle:[NSString stringWithFormat:@"%@秒后重发",strTime] forState:UIControlStateNormal];
                        [self.yanBtn setTitleColor:UIColorRGB(0x929292) forState:UIControlStateNormal];
                        self.yanBtn.userInteractionEnabled = NO;
                    });
                    timeout--;
                }
            });
            dispatch_resume(_timer);
        }
    } failure:^(NSError * _Nullable error) {
        NSLog(@"%@",error);
    }login:^(BOOL isLogin) {
    }];
}


- (IBAction)clickNext:(id)sender {
    if (self.phoneTF.text.length != 11 || self.yanTF.text.length == 0) {
        return;
    }

    WeakSelf
    [HttpRequest postWithURLString:HOST(@"auth/login/phone") parameters:@{@"phone":self.phoneTF.text,@"smsCode":self.yanTF.text} showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
        [MyObject failedPrompt1:responseObject[@"message"]];
        if ([responseObject[@"code"] integerValue] == CODESUCCESS) {
            [HFUser saveToken:responseObject[@"data"][@"token"]];
            [HFUser rememberUserID:responseObject[@"data"][@"userId"]];
            [HFUser saveRefreshToken:responseObject[@"data"][@"refreshToken"]];
            [HFUser rememberUsername:responseObject[@"data"][@"username"]];
            [HFUser rememberNickName:responseObject[@"data"][@"nickName"]];
            [weakSelf saveIcon:responseObject[@"data"][@"avatar"]];
            for (UIViewController * vc in self.navigationController.viewControllers) {
                NSLog(@"class%@",NSStringFromClass([vc class]));
                NSLog(@"myClass%@",self.myClass);
                if ([NSStringFromClass([vc class]) isEqualToString:self.myClass]) {
                    [self.navigationController popToViewController:vc animated:YES];
                }else{
                    for (UIViewController * subVC in vc.childViewControllers) {
                        if ([NSStringFromClass([subVC class]) isEqualToString:self.myClass]) {
                            [self.navigationController popToViewController:vc animated:YES];
                        }
                    }
                }
            }
            if (self.clickForgetBlock) {
                self.clickForgetBlock();
            }
        }
    } failure:^(NSError * _Nullable error) {

    }login:^(BOOL isLogin) {

    }];
}
- (void)saveIcon:(NSString *)image
{
    WeakSelf
    dispatch_queue_t xrQueue = dispatch_queue_create("loadImage", NULL); // 创建GCD线程队列
    dispatch_async(xrQueue, ^{
        // 异步下载图片
        UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:image]]];
        // 主线程刷新UI
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *path_document = NSHomeDirectory();
            //设置一个图片的存储路径
            NSString *imagePath = [path_document stringByAppendingString:@"/Documents/icon.png"];
            //把图片直接保存到指定的路径（同时应该把图片的路径imagePath存起来，下次就可以直接用来取）
            [UIImagePNGRepresentation(img) writeToFile:imagePath atomically:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:FORGETLOGINSUCCESS object:nil];
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        });
    });
}
- (void)clickTap
{
    [self.phoneTF resignFirstResponder];
    [self.yanTF resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.phoneTF) {
        [self.yanTF becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
