//
//  ProtocolViewController.m
//  金融
//
//  Created by Apple on 2017/10/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "ProtocolViewController.h"

@interface ProtocolViewController ()
@property (nonatomic, strong) UIWebView   * webView;
@end

@implementation ProtocolViewController
- (UIWebView *)webView
{
    if (!_webView) {
        _webView = [UIWebView new];
    }
    return _webView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCusNav];
    self.titleL.text = @"用户协议";
    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.top.mas_equalTo(self.barView.mas_bottom);
    }];
    [self networking];
}
- (void)networking
{
    WeakSelf
    [HttpRequest postWithURLString:[NSString stringWithFormat:@"%@%@",HOST(@"app/article/type/"),@"protocol"] parameters:nil showProgressHud:NO success:^(NSDictionary * _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue] == CODESUCCESS) {
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.titleL.text = responseObject[@"data"][@"title"];
                [weakSelf.webView loadHTMLString:responseObject[@"data"][@"body"] baseURL:nil];
            });
        }
    } failure:^(NSError * _Nullable error) {
        
    }login:^(BOOL isLogin) {
        
    }];
}

- (void)setCusNav
{
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_lessThanOrEqualTo(SCREENWIDTH - 100);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

