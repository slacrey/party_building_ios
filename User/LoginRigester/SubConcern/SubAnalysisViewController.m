//
//  SubAnalysisViewController.m
//  金融
//
//  Created by Apple on 2017/9/29.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "SubAnalysisViewController.h"
#import "AnalysisCell.h"
#import "HomeWebViewController.h"

static NSString * cellID = @"AnalysisCellID";
@interface SubAnalysisViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView        * tableView;
@property (nonatomic, strong) NSMutableArray     * dataArray;
@property (nonatomic, assign) NSInteger          page;
@property (nonatomic, assign) BOOL               hasMore;
@end

@implementation SubAnalysisViewController
- (void)setNavBar
{
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    self.titleL.text = @"我的收藏";
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBar];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.barView.mas_bottom);
        if (@available(iOS 11.0, *)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.mas_equalTo(self.view.mas_bottom);
        }
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    self.page = 1;
    self.hasMore = YES;
    [self networking];
}
- (void)footerRefresh
{
    if (self.hasMore) {
        self.page ++;
        [self networking];
    }
}
- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"AnalysisCell" bundle:nil] forCellReuseIdentifier:cellID];
    }
    return _tableView;
}
- (void)networking
{
    WeakSelf
    NSDictionary * dic = @{@"page":[NSNumber numberWithInteger:self.page],
                           @"size":[NSNumber numberWithInteger:20],
                           @"data":@{}
                           };
    [HttpRequest postWithURLString:HOST(@"app/user/analysis/collection/list") parameters:dic showProgressHud:YES success:^(NSDictionary * _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue] == CODESUCCESS) {
            for (NSDictionary * dict in responseObject[@"data"][@"content"]) {
                [self.dataArray addObject:dict];
            }
            [weakSelf.tableView.mj_footer endRefreshing];
            if (weakSelf.page == [responseObject[@"data"][@"totalPages"] integerValue] ) {
                weakSelf.hasMore = NO;
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                weakSelf.tableView.mj_footer.hidden = YES;
            }
            if (weakSelf.dataArray.count > 0) {
                [weakSelf.tableView reloadData];
                weakSelf.tableView.hidden = NO;
                weakSelf.NoDataView.hidden = YES;
            }else{
                weakSelf.tableView.hidden = YES;
                weakSelf.NoDataView.hidden = NO;
            }
        }
    } failure:^(NSError * _Nullable error) {
        weakSelf.tableView.hidden = YES;
        weakSelf.NoDataView.hidden = NO;
        weakSelf.tableView.mj_footer.hidden = YES;
    }login:^(BOOL isLogin) {
        [weakSelf PushLogin];
    }];
}
- (void)reloadCusData
{
    [self networking];
}
- (void)PushLogin
{
    LoginViewController * lgvc = [[LoginViewController alloc] init];
    lgvc.myClass = NSStringFromClass([self class]);
    WeakSelf
    lgvc.LoginSuccessBlock = ^{
        weakSelf.page = 1;
        weakSelf.hasMore = YES;
        [weakSelf.dataArray removeAllObjects];
        [weakSelf networking];
    };
    lgvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:lgvc animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AnalysisCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    [cell setDataWithModel:self.dataArray[indexPath.row]];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 105.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * dict = self.dataArray[indexPath.row];
    HomeWebViewController * hc = [[HomeWebViewController alloc] init];
    hc.cusTitle = dict[@"title"];
    hc.shareUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?id=%@",OS,@"/wap/app/analysis/info",dict[@"id"]]];
    hc.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?token=%@&id=%@&pc=1",OS,@"/wap/app/analysis/info",[HFUser getSaveToken],dict[@"id"]]];
    [self.navigationController pushViewController:hc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
