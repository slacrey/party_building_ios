//
//  RegisterViewController.h
//  金融
//
//  Created by Apple on 2017/9/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIButton *ProtoclBtn;
- (IBAction)clickProtcolAction:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *phoneTF;

@property (weak, nonatomic) IBOutlet UITextField *yanTF;

@property (weak, nonatomic) IBOutlet UITextField *passTF;

- (IBAction)clickRegister:(id)sender;

@end
