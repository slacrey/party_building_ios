//
//  ChangePassWordViewController.h
//  金融
//
//  Created by Apple on 2017/12/19.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePassWordViewController : BaseViewController


@property (weak, nonatomic) IBOutlet UITextField *phoneTF;

@property (weak, nonatomic) IBOutlet UITextField *yanTF;

@property (weak, nonatomic) IBOutlet UITextField *passTF;

- (IBAction)clickRegister:(id)sender;
@end
