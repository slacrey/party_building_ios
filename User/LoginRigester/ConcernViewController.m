//
//  ConcernViewController.m
//  金融
//
//  Created by Apple on 2017/9/29.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "ConcernViewController.h"
#import "InformationViewController.h"
#import "SubAnalysisViewController.h"
#import "CourseViewController.h"

@interface ConcernViewController ()<UIGestureRecognizerDelegate>
@end

@implementation ConcernViewController
- (void)setNavBar
{
    [self.barView addSubview:self.titleL];
    [self.titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.barView);
        make.width.mas_greaterThanOrEqualTo(10);
        make.height.mas_greaterThanOrEqualTo(10);
    }];
    self.titleL.text = @"我的收藏";
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBar];
    
    [self setUpAllChildViewController];
    
    [self setUpDisplayStyle:^(UIColor *__autoreleasing *titleScrollViewBgColor, UIColor *__autoreleasing *norColor, UIColor *__autoreleasing *selColor, UIColor *__autoreleasing *proColor, UIFont *__autoreleasing *titleFont, BOOL *isShowProgressView, BOOL *isOpenStretch, BOOL *isOpenShade, BOOL *isEnable, BOOL * isShowIndex, BOOL * isTop, BOOL * isTabbar) {
        
        *titleScrollViewBgColor = [UIColor whiteColor];
        *titleFont = TITLEFONT;
        *norColor = TEXTColor;
        *selColor = TITLEColor;
        *proColor = TITLEColor;
        *isShowProgressView = YES;                      //是否开启标题下部Pregress指示器
        *isOpenStretch = YES;                           //是否开启指示器拉伸效果
        *isOpenShade = YES;                             //是否开启字体渐变
        *isEnable = NO;
        *isTop = YES;
        *isTabbar = NO;
    }];
}
- (void)setUpAllChildViewController
{
    NSArray *titles = @[@"资讯",@"课程",@"分析"];
    for (NSInteger i = 0; i < titles.count; i++) {
        UIViewController *vc;
        switch (i) {
            case 0:
            {
                InformationViewController * iv = [[InformationViewController alloc] init];
                vc = iv;
            }
                break;
            case 1:
            {
                CourseViewController * cv = [[CourseViewController alloc] init];
                vc = cv;
            }
                break;
            default:
            {
                SubAnalysisViewController * sv = [[SubAnalysisViewController alloc] init];
                vc = sv;
            }
                break;
        }

        vc.title = titles[i];
        vc.view.backgroundColor = BGColor; //随机色
        [self addChildViewController:vc];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
