//
//  LoginViewController.h
//  金融
//
//  Created by Apple on 2018/1/29.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "BaseViewController.h"

@interface LoginViewController : BaseViewController
@property (strong, nonatomic)  UITextField *phoneTF;
@property (strong, nonatomic)  UITextField *passTF;
@property (strong, nonatomic)  UIButton *msgBtn;

@property (nonatomic, copy) void (^LoginSuccessBlock)(void);
@property (nonatomic, copy) NSString  * myClass;
@end
