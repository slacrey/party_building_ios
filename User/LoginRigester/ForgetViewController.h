//
//  ForgetViewController.h
//  金融
//
//  Created by Apple on 2017/9/28.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *yanTF;
@property (nonatomic, copy) NSString  * myClass;
@property (copy, nonatomic) void(^clickForgetBlock)(void);
- (IBAction)clickNext:(id)sender;

@end
