  //
//  TabbarViewController.m
//  financial
//
//  Created by Apple on 2017/9/21.
//  Copyright © 2017年 Apple. All rights reserved.

#import "TabbarViewController.h"

@interface TabbarViewController ()<UITabBarControllerDelegate>

@end

@implementation TabbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setMyTabbar];
}

- (void)setMyTabbar {
    
    NSMutableArray *vcArray=[[NSMutableArray alloc]init];
    NSArray *vcNameArray=@[@"HomeViewController",@"LearRootController",@"ChatRootController",@"MyViewController"];
    NSArray *nameArray=@[@"首页",@"学习",@"交流",@"我的"];
    NSArray *navArray=@[@"tab_root_d",@"tab_learn_d",@"tab_chat_d",@"tab_user_d"];
    NSArray *navSelectArray=@[@"tab_root_s",@"tab_learn_s",@"tab_chat_s",@"tab_user_s"];

    for (int i = 0; i < navArray.count; i++) {
        NSString *vcname = vcNameArray[i];
        UIViewController *rvc = [[(NSClassFromString(vcname))alloc]init];
        UINavigationController *nv=[[UINavigationController alloc]initWithRootViewController:rvc];
        nv.title = nameArray[i];
        [vcArray addObject:nv];       
        [self setTabBarItem:nv andImageName:navArray[i] andSelectedImageName:navSelectArray[i]];
    }
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    self.viewControllers = vcArray;
}

- (void)setTabBarItem:(UINavigationController *)navi andImageName:(NSString *)imageName andSelectedImageName:(NSString *)selectedImageName {
    
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]]; 
    [UITabBar appearance].translucent = NO;
    navi.navigationBar.translucent = NO;
    
    navi.navigationBar.barTintColor = [UIColor whiteColor];
    UIImage * imageN = [[UIImage imageNamed:imageName]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage * selectimage = [[UIImage imageNamed:selectedImageName]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [navi.navigationBar setTitleTextAttributes:@{NSFontAttributeName:TITLEFONT}];
    [navi.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:TITLEColor}];
    navi.tabBarItem.image = imageN;
    navi.tabBarItem.selectedImage = selectimage;
  
    [navi.tabBarItem setTitleTextAttributes:@{NSFontAttributeName:INSFONT,NSForegroundColorAttributeName:RedColor} forState:UIControlStateNormal];
    [navi.tabBarItem setTitleTextAttributes:@{NSFontAttributeName:INSFONT,NSForegroundColorAttributeName:TITLEColor} forState:UIControlStateSelected];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
