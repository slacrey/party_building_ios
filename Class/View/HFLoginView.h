//
//  HFLoginView.h
//  金融
//
//  Created by Apple on 2018/1/3.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HFLoginView : UIView
@property (nonatomic, copy) void(^clickPushBlock)(NSInteger index);
+ (instancetype)shareInit;
- (void)dismiss;
- (void)show;
@end
