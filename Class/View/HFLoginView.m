//
//  HFLoginView.m
//  金融
//
//  Created by Apple on 2018/1/3.
//  Copyright © 2018年 Apple. All rights reserved.
//

#import "HFLoginView.h"

@interface HFLoginView()
@property (nonatomic, strong) UIView    * bgView;
@property (nonatomic, strong) UIView    * blackView;
@property (nonatomic, strong) UIButton  * rigestBtn;
@property (nonatomic, strong) UIButton  * loginBtn;
@end
@implementation HFLoginView

+ (instancetype)shareInit
{
    static HFLoginView * popView;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        popView = [[HFLoginView alloc] init];
    });
    return popView;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.frame = SCREENFRAME;
        [self setup];
    }
    return self;
}
- (UIView *)blackView
{
    if (!_blackView) {
        _blackView = [[UIView alloc] initWithFrame:SCREENFRAME];
        _blackView.backgroundColor = [UIColor blackColor];
        _blackView.alpha = 0.6;
    }
    return _blackView;
}
- (UIView *)bgView
{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.layer.cornerRadius = 10.f;
        _bgView.layer.masksToBounds = YES;
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}
- (UIButton *)rigestBtn
{
    if (!_rigestBtn) {
        _rigestBtn = [UIButton new];
        _rigestBtn.tag = 0;
        _rigestBtn.layer.cornerRadius = 5.f;
        _rigestBtn.layer.masksToBounds = YES;
        [_rigestBtn setTitle:@"注册" forState:UIControlStateNormal];
        [_rigestBtn setBackgroundColor:UIColorRGB(0xe6e6e6)];
        [_rigestBtn setTitleColor:UIColorRGB(0x999999) forState:UIControlStateNormal];
        [_rigestBtn addTarget:self action:@selector(clickBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rigestBtn;
}
- (UIButton *)loginBtn
{
    if (!_loginBtn) {
        _loginBtn = [UIButton new];
        _loginBtn.tag = 1;
        _loginBtn.layer.cornerRadius = 3.f;
        _loginBtn.layer.masksToBounds = YES;
        [_loginBtn setBackgroundColor:UIColorRGB(0x2772ff)];
        [_loginBtn setTitleColor:UIColorRGB(0xffffff) forState:UIControlStateNormal];
        [_loginBtn setTitle:@"登录" forState:UIControlStateNormal];
        [_loginBtn addTarget:self action:@selector(clickBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _loginBtn;
}
- (void)clickBtnAction:(UIButton *)sender
{
    [self dismiss];
    if (self.clickPushBlock) {
        self.clickPushBlock(sender.tag);
    }
}
- (void)setup
{
    [self addSubview:self.blackView];
    
    [self addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_offset(CGSizeMake(300 * SizeWidth, 225 * SizeWidth));
        make.center.mas_equalTo(self);
    }];
    [self.bgView addSubview:self.rigestBtn];
    [self.bgView addSubview:self.loginBtn];
    [self.rigestBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.bgView.mas_left).offset(20);
        make.bottom.mas_equalTo(self.bgView.mas_bottom).offset(-15);
        make.height.mas_offset(30 * SizeWidth);
        make.width.mas_offset(120 * SizeWidth);
    }];
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.bgView.mas_right).offset(-20);
        make.bottom.mas_equalTo(self.bgView.mas_bottom).offset(-15);
        make.height.mas_offset(30 * SizeWidth);
        make.width.mas_offset(120 * SizeWidth);
    }];
    UILabel * titleL = [UILabel new];
    titleL.font = FONT(17);
    titleL.textColor = UIColorRGB(0x383838);
    titleL.textAlignment = NSTextAlignmentCenter;
    titleL.text = @"您还未登录";
    [self.bgView addSubview:titleL];
    [titleL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.bgView);
        make.height.mas_offset(44);
    }];
    
    UILabel * line = [UILabel new];
    line.backgroundColor = LINEColor;
    [self.bgView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.bgView);
        make.top.mas_equalTo(titleL.mas_bottom);
        make.height.mas_offset(0.5);
    }];
    
    UILabel * subL = [UILabel new];
    subL.font = FONT(14);
    subL.textColor = UIColorRGB(0x666666);
    subL.textAlignment = NSTextAlignmentCenter;
    subL.text = @"(请登录/注册再进行操作)";
    [self.bgView addSubview:subL];
    [subL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.bgView);
        make.top.mas_equalTo(line.mas_bottom).offset(10);
        make.height.mas_offset(20);
    }];
    UIImageView * imagV = [UIImageView new];
    [self.bgView addSubview:imagV];
    [imagV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.bgView);
        make.top.mas_equalTo(subL.mas_bottom).offset(5);
        make.bottom.mas_equalTo(self.rigestBtn.mas_top).offset(-5);
        make.width.equalTo(imagV.mas_height);
    }];
    imagV.image = [UIImage imageNamed:@"login_pushBG"];
    UILabel * langL = [UILabel new];
    langL.backgroundColor = [UIColor whiteColor];
    [self addSubview:langL];
    [langL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bgView.mas_bottom);
        make.size.mas_offset(CGSizeMake(1, 60 * SizeWidth));
        make.centerX.mas_equalTo(self);
    }];
    UIImageView * closeV = [UIImageView new];
    closeV.image = [UIImage imageNamed:@"close_login"];
    [self addSubview:closeV];
    [closeV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.size.mas_offset(CGSizeMake(24, 24));
        make.top.mas_equalTo(langL.mas_bottom).offset(-1);
    }];
    UIButton * closeBtn = [UIButton new];
    [closeBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:closeBtn];
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self);
        make.size.mas_offset(CGSizeMake(80, 80));
        make.top.mas_equalTo(self.bgView.mas_bottom).offset(24);
    }];
}

- (void)dismiss
{
    __weak typeof(self)  weakself = self;
    [UIView animateWithDuration:1.0 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:1 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
        [weakself removeFromSuperview];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)show
{
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    self.center = CGPointMake(SCREENWIDTH * 0.5, SCREENHEIGHT * 0.5);
    [UIView animateWithDuration:1.0 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:1 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        
    } completion:^(BOOL finished) {
        
    }];
}
@end

