//
//  AddLineBtn.m
//  金融
//
//  Created by Apple on 2017/9/27.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "AddLineBtn.h"
@interface AddLineBtn()
@property (nonatomic, strong) UILabel * lineL;
@end
@implementation AddLineBtn
- (instancetype)init
{
    if (self = [super init]) {
        [self addSubview:self.lineL];
    }
    return self;
}
- (UILabel *)lineL
{
    if (!_lineL) {
        _lineL = [UILabel new];
        _lineL.backgroundColor = TITLEColor;
        _lineL.hidden = YES;
    }
    return _lineL;
}
- (void)setLineBackColor:(UIColor *)color
{
    self.lineL.backgroundColor = color;
}
- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    self.lineL.hidden = selected ? NO : YES;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.lineL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(10);
        make.right.mas_equalTo(self).mas_offset(-10);
        make.bottom.mas_equalTo(self);
        make.height.mas_offset(2);
    }];
}
@end
