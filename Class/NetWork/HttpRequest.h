//
//  HttpRequest.h
//  CardsGame
//
//  Created by PSY on 2016/12/8.
//  Copyright © 2016年 myGame. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UploadParam.h"
#import "AFNetworking.h"

@interface HttpRequest : NSObject


/**
 *  发送get请求
 *
 *  @param URLString  请求的网址字符串
 *  @param parameters 请求的参数
 *  @param success    请求成功的回调
 *  @param failure    请求失败的回调
 
 
 
 
 */
+ (void)getWithURLString:(NSString *_Nullable)URLString
              parameters:(id _Nullable )parameters
         showProgressHud:(BOOL)show
                 success:(void (^_Nullable)(NSDictionary * _Nullable responseObject))success
                 failure:(void (^_Nullable)(NSError * _Nullable error))failure;



/**
 *  发送post请求
 *
 *  @param URLString  请求的网址字符串
 *  @param parameters 请求的参数
 *  @param success    请求成功的回调
 *  @param failure    请求失败的回调
 */
+ (void)postWithURLString:(NSString *_Nullable)URLString
               parameters:(id _Nullable )parameters
          showProgressHud:(BOOL)show
                  success:(void (^_Nullable)(NSDictionary * _Nullable responseObject))success
                  failure:(void (^_Nullable)(NSError * _Nullable error))failure
                    login:(void (^_Nullable)(BOOL isLogin))login;

+ (void)postSimWithURLString:(NSString *_Nullable)URLString
               parameters:(id _Nullable )parameters
          showProgressHud:(BOOL)show
                  success:(void (^_Nullable)(NSDictionary * _Nullable responseObject))success
                  failure:(void (^_Nullable)(NSError * _Nullable error))failure;
/**
 *  发送网络请求
 *
 *  @param URLString   请求的网址字符串
 *  @param parameters  请求的参数
 *  @param type        请求的类型
 *  @param success     请求的结果
 */
+ (void)requestWithURLString:(NSString *_Nullable)URLString
                  parameters:(id _Nullable )parameters
             showProgressHud:(BOOL)show
                        type:(HttpRequestType)type
                     success:(void (^_Nullable)(NSDictionary * _Nullable responseObject))success
                     failure:(void (^_Nullable)(NSError * _Nullable error))failure;

/**
 *  上传图片
 *
 *  @param URLString   上传图片的网址字符串
 *  @param parameters  上传图片的参数
 *  @param uploadParam 上传图片的信息
 *  @param success     上传成功的回调
 *  @param failure     上传失败的回调
 */
+ (void)uploadWithURLString:(NSString *_Nullable)URLString
                 parameters:(id _Nullable )parameters
            showProgressHud:(BOOL)show
                uploadParam:(UploadParam *_Nullable)uploadParam
                    success:(void (^_Nullable)(NSDictionary * _Nullable responseObject))success
                    failure:(void (^_Nullable)(NSError * _Nullable error))failure;


//+ (void)postWithURLString:(NSString *_Nullable)URLString
//               parameters:(id _Nullable )parameters
//constructingBodyWithBlock:(void <AFMultipartFormData> formData)block
//                 progress:(nullable void (^)(NSProgress * _Nonnull))uploadProgress
//                  success:(void (^_Nullable)(NSDictionary * _Nullable responseObject))success
//                  failure:(void (^_Nullable)(NSError * _Nullable error))failure;



@end
