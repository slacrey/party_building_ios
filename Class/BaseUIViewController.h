//
//  BaseUIViewController.h
//  金融
//
//  Created by Apple on 2017/11/21.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseUIViewController : UIViewController
@property (nonatomic, strong) UIView      * NoDataView;
@property (nonatomic, strong) UILabel     * reLoad;
- (void)reloadCusData;
@end
