//
//  WriteMessageViewController.h
//  金融
//
//  Created by Apple on 2017/11/10.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSUInteger,WriteType) {
    WriteCouserType = 0,
    WriteInfoType
};
@interface WriteMessageViewController : BaseViewController
@property(strong,nonatomic) UIScrollView *scrollView;
@property(assign,nonatomic) WriteType           type;
@property(strong,nonatomic) NSDictionary      * dict;
@property(nonatomic,strong) UIView *noteTextBackgroudView;

//文字个数提示label
@property(nonatomic,strong) UILabel *textNumberLabel;

//文字说明
@property(nonatomic,strong) UILabel *explainLabel;

//提交按钮
@property(nonatomic,strong) UIButton *submitBtn;
@property (nonatomic, copy) void(^clickMessageBlock)(void);
@end
