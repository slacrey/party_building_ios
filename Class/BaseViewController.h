//
//  BaseViewController.h
//  financial
//
//  Created by Apple on 2017/9/21.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController
@property (nonatomic, strong) UILabel     * titleL;
@property (nonatomic, strong) UILabel     * reLoad;
@property (nonatomic, strong) UIButton    * backBtn;
@property (nonatomic, strong) UIView      * barView;
@property (nonatomic, strong) UIView      * statusView;
@property (nonatomic, strong) UIButton    * searchBtn;
@property (nonatomic, strong) UIView      * NoDataView;
- (void)setHideBackBtn:(BOOL)isHidden;
- (void)showSearchBtn;
- (void)clickBackAction;
- (void)PushLogin;
- (void)PushRigest;
- (void)clickSearchAction;
- (void)reloadCusData;
@end
