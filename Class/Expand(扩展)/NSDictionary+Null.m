//
//  NSDictionary+Null.m
//  Bbuilding
//
//  Created by apple on 2017/5/5.
//  Copyright © 2017年 HF. All rights reserved.
//

#import "NSDictionary+Null.h"

@implementation NSDictionary (Null)
- (id)dictNullKey:(NSString *)key
{
    if ([[self objectForKey:key] isKindOfClass:[NSNull class]] || [self objectForKey:key] == nil) {
        return @"";
    }else{
        return [self objectForKey:key];
    }
}

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                         
                                                        options:NSJSONReadingMutableContainers
                         
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}
@end
