//
//  HFUser.m
//  Bbuilding
//
//  Created by apple on 2017/4/29.
//  Copyright © 2017年 HF. All rights reserved.
//

#import "HFUser.h"

static HFUser * user = nil;
@implementation HFUser

+ (HFUser *)currentUser
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        user = [[HFUser alloc] init];
    });
    return user;
}

+ (void)rememberUserID:(NSString *)userID
{
    [[NSUserDefaults standardUserDefaults] setObject:userID forKey:ID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastUserID
{
    NSString * userID = [[NSUserDefaults standardUserDefaults] objectForKey:ID];
    if (!userID && userID.length == 0) {
        return nil;
    }
    return userID;
}

+ (void)rememberOpenID:(NSString *)OpenID
{
    [[NSUserDefaults standardUserDefaults] setObject:OpenID forKey:WXOpenID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+ (NSString *)lastOpenID
{
    NSString * OpenID = [[NSUserDefaults standardUserDefaults] objectForKey:WXOpenID];
    if (!OpenID && OpenID.length == 0) {
        return nil;
    }
    return OpenID;
}
+ (void)rememberUsername:(NSString *)username
{
    [[NSUserDefaults standardUserDefaults] setObject:username forKey:Name];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastUsername
{
    NSString * username = [[NSUserDefaults standardUserDefaults] objectForKey:Name];
    if (!username && username.length == 0) {
        return nil;
    }
    return username;
}
+ (void)rememberNickName:(NSString *)nickName
{
    [[NSUserDefaults standardUserDefaults] setObject:nickName forKey:NickName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastNickName
{
    NSString * nickName = [[NSUserDefaults standardUserDefaults] objectForKey:NickName];
    if (!nickName && nickName.length == 0) {
        return nil;
    }
    return nickName;
}
// 保存用户Icon
+ (void)rememberIcon:(NSString *)icon
{
    [[NSUserDefaults standardUserDefaults] setObject:icon forKey:IconKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastIcon
{
    NSString * icon = [[NSUserDefaults standardUserDefaults] objectForKey:IconKey];
    if (!icon && icon.length == 0) {
        return nil;
    }
    return icon;
}
// 保存用户City
+ (void)rememberCity:(NSString *)city
{
    [[NSUserDefaults standardUserDefaults] setObject:city forKey:CityKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastCity
{
    NSString * city = [[NSUserDefaults standardUserDefaults] objectForKey:CityKey];
    if (!city && city.length == 0) {
        return nil;
    }
    return city;
}
// 保存JMessage用户名
+ (void)rememberJMUserName:(NSString *)name
{
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:JMuserName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastJMName
{
    NSString * JMName = [[NSUserDefaults standardUserDefaults] objectForKey:JMuserName];
    if (!JMName && JMName.length == 0) {
        return nil;
    }
    return JMName;
}

+ (void)rememberLoginType:(NSString *)type
{
    [[NSUserDefaults standardUserDefaults] setObject:type forKey:LoginType];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastLoginType
{
    NSString * type = [[NSUserDefaults standardUserDefaults] objectForKey:LoginType];
    if (!type && type.length == 0) {
        return nil;
    }
    return type;
}

+ (void)saveToken:(NSString *)token
{
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:LoginToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)getSaveToken
{
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:LoginToken];
    if (!token && token.length == 0) {
        return @"";
    }
    return token;
}
+ (void)saveRefreshToken:(NSString *)token
{
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:LoginRefreshToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)getSaveRefreshToken
{
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:LoginRefreshToken];
    if (!token && token.length == 0) {
        return @"";
    }
    return token;
}
+ (void)deleteToken
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:IconKey];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:Name];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LoginToken];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LoginRefreshToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (UIImage *)getIcon
{
    NSString *path_document = NSHomeDirectory();
    
    NSString *imagePath = [path_document stringByAppendingString:@"/Documents/icon.png"];
    
    UIImage *getimage = [UIImage imageWithContentsOfFile:imagePath];
    
    return getimage;
}
+ (void)deleteIcon
{
    NSString *path_document = NSHomeDirectory();
    NSString *imagePath = [path_document stringByAppendingString:@"/Documents/icon.png"];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    BOOL bRet = [fileMgr fileExistsAtPath:imagePath];
    if (bRet) {
        [fileMgr removeItemAtPath:imagePath error:nil];
    }
}
+ (void)saveDate:(NSDate *)date
{
    [[NSUserDefaults standardUserDefaults] setObject:date forKey:LoginDate];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSDate *)getSaveDate
{
    NSDate *date = [[NSUserDefaults standardUserDefaults] objectForKey:LoginDate];
    if (!date) {
        return nil;
    }
    return date;
}
@end

