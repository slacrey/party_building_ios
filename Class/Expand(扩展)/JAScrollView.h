//
//  JAScrollView.h
//  AutomobileAccessories
//
//  Created by JA on 2017/3/17.
//  Copyright © 2017年  All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ImageViewClick)(NSInteger index);

@interface JAScrollView : UIView
{
    NSInteger currentPage;
    BOOL flag;
    NSInteger scrollTopicFlag;
    UIScrollView *adScrollView;
    NSInteger picCount;
    UILabel *_adsTitleLb;
}
@property (nonatomic, strong)UIImageView *imageView;
@property (nonatomic, strong)NSArray *imagesArray;
@property (nonatomic, copy)ImageViewClick block;
@property (nonatomic, strong)NSTimer *timer;
@property (nonatomic, strong)UIPageControl *pageControl;

- (instancetype)initWithFrame:(CGRect)frame;

- (void)initView;

- (void)setImagesArray:(NSArray *)imagesArray;
@end
