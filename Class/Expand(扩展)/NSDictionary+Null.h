//
//  NSDictionary+Null.h
//  Bbuilding
//
//  Created by apple on 2017/5/5.
//  Copyright © 2017年 HF. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Null)
- (id)dictNullKey:(NSString *)key;
//JSON字符串转字典
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;
@end
