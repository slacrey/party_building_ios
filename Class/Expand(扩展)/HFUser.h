//
//  HFUser.h
//  Bbuilding
//
//  Created by apple on 2017/4/29.
//  Copyright © 2017年 HF. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ID              @"ID"
#define WXOpenID        @"openID"
#define Name            @"Name"
#define NickName        @"NickName"
#define IconKey         @"IconKey"
#define CityKey         @"CityKey"
#define LoginToken      @"LoginToken"
#define LoginRefreshToken    @"LoginRefreshToken"
#define LoginType       @"LoginType"
#define LoginDate       @"LoginDate"
#define JMuserName      @"JMuserName"

@interface HFUser : NSObject


+ (HFUser *)currentUser;
// 保存用户ID
+ (void)rememberUserID:(NSString *)userID;

+ (NSString *)lastUserID;
// 保存用户openID
+ (void)rememberOpenID:(NSString *)OpenID;

+ (NSString *)lastOpenID;
// 保存用户名
+ (void)rememberUsername:(NSString *)username;

+ (NSString *)lastUsername;
// 保存别名
+ (void)rememberNickName:(NSString *)nickName;

+ (NSString *)lastNickName;
// 保存用户Icon
+ (void)rememberIcon:(NSString *)icon;

+ (NSString *)lastIcon;

// 保存JMessage用户名
+ (void)rememberJMUserName:(NSString *)name;

+ (NSString *)lastJMName;

// 保存用户City
+ (void)rememberCity:(NSString *)city;

+ (NSString *)lastCity;

+ (void)rememberLoginType:(NSString *)type;

+ (NSString *)lastLoginType;
// 保存用户token
+ (void)saveToken:(NSString *)token;

+ (NSString *)getSaveToken;

+ (void)saveRefreshToken:(NSString *)token;

+ (NSString *)getSaveRefreshToken;

+ (void)deleteToken;

+ (UIImage *)getIcon;

+ (void)deleteIcon;

+ (void)saveDate:(NSDate *)date;

+ (NSDate *)getSaveDate;
@end
