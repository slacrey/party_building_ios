//
//  BaseUIViewController.m
//  金融
//
//  Created by Apple on 2017/11/21.
//  Copyright © 2017年 Apple. All rights reserved.
//

#import "BaseUIViewController.h"
//#import <UMAnalytics/MobClick.h>

@interface BaseUIViewController ()

@end

@implementation BaseUIViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [MobClick beginLogPageView:NSStringFromClass(self.class)]; //("Pagename"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
//    [SVProgressHUD dismiss];
//    [super viewWillDisappear:animated];
//    [MobClick endLogPageView:NSStringFromClass(self.class)];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    self.view.backgroundColor = BGColor;
    [self addCusNoData];
}
- (UIView *)NoDataView
{
    if (!_NoDataView) {
        _NoDataView = [UIView new];
        _NoDataView.backgroundColor = BGColor;
    }
    return _NoDataView;
}
- (void)addCusNoData
{
    [self.view addSubview:self.NoDataView];
    [self.NoDataView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view);
        make.left.mas_equalTo(self.view);
//        make.width.mas_offset(SCREENWIDTH);
        if (@available(iOS 11.0, *)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.mas_equalTo(self.view.mas_bottom);
        }
    }];
    UIImageView * imagV = [[UIImageView alloc] init];
    imagV.image = [UIImage imageNamed:@"no_data"];
    [self.NoDataView addSubview:imagV];
    
    UILabel * label = [UILabel new];
    label.text = @"暂无数据";
    label.font = FONT(16);
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = SUBTEXTColor;
    [self.NoDataView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.NoDataView);
//        make.size.mas_offset(CGSizeMake(SCREENWIDTH, 20));
    }];
    
    [imagV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(label.mas_top).offset(-15);
        make.centerX.mas_equalTo(self.NoDataView);
        make.size.mas_offset(CGSizeMake(100, 65.5));
    }];
    
    self.reLoad = [UILabel new];
    [self.NoDataView addSubview:self.reLoad];
    [self.reLoad mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label.mas_bottom).offset(5);
        make.centerX.mas_equalTo(self.NoDataView);
        make.size.mas_offset(CGSizeMake(140, 41));
    }];
    self.reLoad.text = @"请点击屏幕刷新";
    self.reLoad.font = FONT(16);
    self.reLoad.textAlignment = NSTextAlignmentCenter;
    self.reLoad.textColor = SUBTEXTColor;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reloadCusData)];
    [self.NoDataView addGestureRecognizer:tap];
    self.NoDataView.userInteractionEnabled = YES;
    self.NoDataView.hidden = YES;
}
- (void)reloadCusData
{
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
