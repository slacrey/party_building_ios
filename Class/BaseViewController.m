//
//  BaseViewController.m
//  financial
//
//  Created by Apple on 2017/9/21.
//  Copyright © 2017年 Apple. All rights reserved.

#import "BaseViewController.h"
//#import "SearchBaseViewController.h"
//#import <UMAnalytics/MobClick.h>

#define H 44
@interface BaseViewController ()<UIGestureRecognizerDelegate>
@end

@implementation BaseViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [MobClick beginLogPageView:NSStringFromClass(self.class)]; //("Pagename"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [MobClick endLogPageView:NSStringFromClass(self.class)];
}
- (UILabel *)titleL
{
    if (!_titleL) {
        _titleL = [UILabel new];
        _titleL.font = TITLEFONT;
        _titleL.textColor = SELECTCOLOR;
    }
    return _titleL;
}
- (UIView *)barView
{
    if (!_barView) {
        _barView = [UIView new];
        _barView.backgroundColor = BARColor;
    }
    return _barView;
}
- (UIView *)statusView
{
    if (!_statusView) {
        _statusView = [UIView new];
        _statusView.backgroundColor = BARColor;
    }
    return _statusView;
}
- (UIView *)NoDataView
{
    if (!_NoDataView) {
        _NoDataView = [UIView new];
        _NoDataView.backgroundColor = BGColor;
    }
    return _NoDataView;
}
- (void)addCusNoData
{
    [self.view addSubview:self.NoDataView];
    [self.NoDataView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.barView.mas_bottom);
        make.left.mas_equalTo(self.view);
//        make.width.mas_offset(SCREENWIDTH);
        if (@available(iOS 11.0, *)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.mas_equalTo(self.view.mas_bottom);
        }
    }];
    UIImageView * imagV = [[UIImageView alloc] init];
    imagV.image = [UIImage imageNamed:@"no_data"];
    [self.NoDataView addSubview:imagV];

    UILabel * label = [UILabel new];
    label.text = @"暂无数据";
    label.font = FONT(16);
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = SUBTEXTColor;
    [self.NoDataView addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.NoDataView);
//        make.size.mas_offset(CGSizeMake(SCREENWIDTH, 20));
    }];
    
    [imagV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(label.mas_top).offset(-15);
        make.centerX.mas_equalTo(self.NoDataView);
        make.size.mas_offset(CGSizeMake(100, 65.5));
    }];

    self.reLoad = [UILabel new];
    [self.NoDataView addSubview:self.reLoad];
    [self.reLoad mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label.mas_bottom).offset(5);
        make.centerX.mas_equalTo(self.NoDataView);
        make.size.mas_offset(CGSizeMake(140, 41));
    }];
    self.reLoad.text = @"请点击屏幕刷新";
    self.reLoad.font = FONT(16);
    self.reLoad.textAlignment = NSTextAlignmentCenter;
    self.reLoad.textColor = SUBTEXTColor;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reloadCusData)];
    [self.NoDataView addGestureRecognizer:tap];
    self.NoDataView.userInteractionEnabled = YES;
    self.NoDataView.hidden = YES;
}
- (void)reloadCusData
{
}
- (UIButton *)backBtn
{
    if (!_backBtn) {
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setFrame:CGRectMake(0, 0, 44, 44)];
        [_backBtn setImage:[UIImage imageNamed:@"Return"] forState:UIControlStateNormal];
        [_backBtn addTarget:self action:@selector(clickBackAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backBtn;
}
- (void)clickBackAction
{
//    [SVProgressHUD dismiss];
    [self.navigationController popViewControllerAnimated:YES];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationController.navigationBar.hidden = YES;
    self.view.backgroundColor = BGColor;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }

    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    [self.view addSubview:self.statusView];
    [self.statusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(self.view);
        make.height.mas_offset(statusBar.frame.size.height);
    }];
    [self.view addSubview:self.barView];

    [self.barView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.statusView);
        make.top.mas_equalTo(self.statusView.mas_bottom).offset(0);
        make.height.mas_offset(self.navigationController.navigationBar.frame.size.height);
    }];

    [self.barView addSubview:self.backBtn];
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.barView);
        make.size.mas_offset(CGSizeMake(44, 44));
        make.centerY.mas_equalTo(self.barView);
    }];
    [self addCusNoData];
}
- (void)setHideBackBtn:(BOOL)isHidden
{
    self.backBtn.hidden = isHidden;
}
- (UIButton *)searchBtn
{
    if (!_searchBtn) {
        _searchBtn = [[UIButton alloc] init];
        [_searchBtn setImage:[UIImage imageNamed:@"search_base"] forState:UIControlStateNormal];
        [_searchBtn addTarget:self action:@selector(clickSearchAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchBtn;
}
- (void)clickSearchAction
{
//    SearchBaseViewController * sv = [[SearchBaseViewController alloc] init];
//    sv.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:sv animated:NO];
}
- (void)showSearchBtn
{
    self.backBtn.hidden = YES;
    [self.barView addSubview:self.searchBtn];
    [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.barView);
        make.size.mas_offset(CGSizeMake(44, 44));
        make.centerY.mas_equalTo(self.barView);
    }];
}
- (void)PushLogin
{
//    LoginViewController * lgvc = [[LoginViewController alloc] init];
//    lgvc.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:lgvc animated:YES];
}
- (void)PushRigest
{
//    RegisterViewController * rgvc = [[RegisterViewController alloc] init];
//    rgvc.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:rgvc animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
