//
//  NSString+Date.h
//  Bbuilding
//
//  Created by apple on 2017/5/22.
//  Copyright © 2017年 HF. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Date)
+ (NSString *)timeWithTimeIntervalString:(NSString *)timeString;
+ (NSString *)timeWithAllTimeIntervalString:(NSString *)timeString;
+ (NSString *)timeWithDayIntervalString:(NSString *)timeString;
+ (NSString *)timeWithYMDIntervalString:(NSString *)timeString;
@end
