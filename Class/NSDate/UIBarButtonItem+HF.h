//
//  UIBarButtonItem+HF.h
//  Bbuilding
//
//  Created by apple on 2017/4/28.
//  Copyright © 2017年 HF. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    BarButtonItemleft,
    BarButtonItemright
}BarButtonItemType;

@interface UIBarButtonItem (HF)
+ (instancetype)itemWithImage:(NSString *)image direction:(BarButtonItemType)direction tag:(NSInteger)tag target:(id)target action:(SEL)action;

+ (instancetype)itemWithTitle:(NSString *)title direction:(BarButtonItemType)direction tag:(NSInteger)tag target:(id)target action:(SEL)action;

+ (instancetype)itemWithTitle:(NSString *)title  unSelect:(NSString *)unSelect direction:(BarButtonItemType)direction tag:(NSInteger)tag border:(CGFloat)border borderColor:(UIColor *)borderColor cornerRadius:(CGFloat)cornerRadius target:(id)target action:(SEL)action;
@end
