//
//  UIBarButtonItem+HF.m
//  Bbuilding
//
//  Created by apple on 2017/4/28.
//  Copyright © 2017年 HF. All rights reserved.
//

#import "UIBarButtonItem+HF.h"

@implementation UIBarButtonItem (HF)
+ (instancetype)itemWithImage:(NSString *)image direction:(BarButtonItemType)direction tag:(NSInteger)tag target:(id)target action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [button setFrame:CGRectMake(0, 0, 44, 44)];
    button.imageEdgeInsets = direction ? UIEdgeInsetsMake(9, 18, 9, 0) : UIEdgeInsetsMake(9, 0, 9, 18);
    button.tag = tag;
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[self alloc] initWithCustomView:button];
}

+ (instancetype)itemWithTitle:(NSString *)title direction:(BarButtonItemType)direction tag:(NSInteger)tag target:(id)target action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setFrame:CGRectMake(0, 0, 44, 44)];
    button.tag = tag;
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[self alloc] initWithCustomView:button];
}
+ (instancetype)itemWithTitle:(NSString *)title unSelect:(NSString *)unSelect direction:(BarButtonItemType)direction tag:(NSInteger)tag border:(CGFloat)border borderColor:(UIColor *)borderColor cornerRadius:(CGFloat)cornerRadius target:(id)target action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:borderColor forState:UIControlStateNormal];
    [button setFrame:CGRectMake(0, 8, 54, 28)];
    if (cornerRadius != 0) {
        button.layer.borderWidth = border;
        button.layer.borderColor = borderColor.CGColor;
        button.layer.cornerRadius = cornerRadius;
        button.layer.masksToBounds = YES;
    }
    if (unSelect) {
        [button setTitle:unSelect forState:UIControlStateSelected];
    }
    button.tag = tag;
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[self alloc] initWithCustomView:button];
}
@end
